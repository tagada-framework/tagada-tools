#!/usr/bin/python3

import argparse
import shutil

import shell
from shell import Bin
from utils import eprint, download_and_execute


def install_cargo():
    if shutil.which("cargo") is None:
        eprint("Installing cargo")
        download_and_execute("https://sh.rustup.rs", "sh.rustup.rs", "-y")


def generate_env():
    env_sh = f"""
    #!/usr/bin/bash
    # Tools
    export PATH="$TAGADA_HOME/tagada-tools/target/release:$PATH"
    """
    with open("env.sh", "w") as env_sh_file:
        env_sh_file.write(env_sh.strip())


if __name__ == "__main__":
    parser = argparse.ArgumentParser("install.py")
    parser.add_argument("-v", "--verbose", help="enable verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        shell.VERBOSITY = True

    # Install cargo
    install_cargo()
    cargo = Bin("cargo")
    cargo.build = cargo % "build"
    cargo.build.release = cargo.build % "--release"
    # Build Tagada-Tools
    cargo.build.release()
    # Generate env.sh file
    generate_env()
