import os
from contextlib import contextmanager
from typing import Union, List

from utils import eprint

VERBOSITY = False
SOURCES = []


@contextmanager
def extended_path(*extensions):
    assert len(extensions) > 0
    previous_path = None
    if "PATH" in os.environ:
        previous_path = os.environ["PATH"]
        os.environ["PATH"] = os.pathsep.join(extensions) + os.pathsep + previous_path
    else:
        os.environ["PATH"] = os.pathsep.join(extensions)

    yield

    if previous_path is not None:
        os.environ["PATH"] = previous_path
    else:
        del os.environ["PATH"]


@contextmanager
def cd(path):
    previous_path = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(previous_path)


def exit_on_failure(ret_code):
    exit(ret_code)


def source(path):
    path = os.path.expanduser(path)
    path = os.path.expandvars(path)
    if os.path.isfile(path):
        return f". {path}"
    return None


class filtermap:
    def __init__(self, fn, iterable):
        self._fn = fn
        self._iterable = iterable
        self._iterator = None

    def __next__(self):
        fn_next = self._fn(next(self._iterator))
        while fn_next is None:
            fn_next = self._fn(next(self._iterator))
        return fn_next

    def __iter__(self):
        self._iterator = iter(self._iterable)
        return self


def shell(cmd, error_handler=exit_on_failure):
    if VERBOSITY:
        eprint(cmd)
    sources = list(filtermap(source, SOURCES))
    ret_code = os.system("\n".join(sources + [cmd]))
    if ret_code != 0:
        error_handler(ret_code)
    return ret_code


class Bin:
    def __init__(self, *args: [str | List[str]]):
        self.args = []
        for arg in args:
            if isinstance(arg, Bin):
                self.args += arg.args
            else:
                self.args.append(arg)

    def __mod__(self, sub_command: Union["Bin", str]) -> "Bin":
        return Bin(self, sub_command)

    def __call__(self, *argument: str, error_handler=exit_on_failure):
        return shell(" ".join(self.args + list(argument)), error_handler)
