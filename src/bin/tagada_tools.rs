use std::io;
use clap::{Parser, Subcommand};
use tagada_lib::tools::gen_graphviz::{bin_generate_graphviz, GenerateGraphvizArgs};
use simple_logger::SimpleLogger;
use tagada_lib::tools::evaluate::differential_distinguisher::{evaluate_differential_distinguisher, EvaluateDifferentialDistinguisherArgs};
use tagada_lib::tools::search::best_differential_characteristic::{BestDifferentialCharacteristicArgs, bin_best_differential_characteristic};
use tagada_lib::tools::search::best_truncated_differential_characteristic::{BestTruncatedDifferentialCharacteristicArgs, bin_best_truncated_differential_characteristic};
use tagada_lib::tools::test::test_vectors::{bin_test_vectors, TestVectorArgs};
use tagada_lib::tools::transform::derive::{bin_derive, DeriveArgs};
use tagada_lib::tools::transform::single_key::{bin_single_key, SingleKeyArgs};
use tagada_lib::tools::transform::truncate_differential::{truncate_differential, TruncateDifferentialArgs};

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    action: Action
}

#[derive(Subcommand)]
enum Action {
    #[clap(subcommand)]
    Transform(Transform),
    GenerateGraphviz(GenerateGraphvizArgs) ,
    #[clap(subcommand)]
    Evaluate(Evaluate),
    #[clap(subcommand)]
    Test(Test),
    #[clap(subcommand)]
    Search(Search),
}

#[derive(Subcommand)]
enum Transform {
    SingleKey(SingleKeyArgs),
    Derive(DeriveArgs),
    TruncateDifferential(TruncateDifferentialArgs),
}

#[derive(Subcommand)]
enum Search {
    BestDifferentialCharacteristic(BestDifferentialCharacteristicArgs),
    BestTruncatedDifferentialCharacteristic(BestTruncatedDifferentialCharacteristicArgs),
}

#[derive(Subcommand)]
enum Evaluate {
    DifferentialDistinguisher(EvaluateDifferentialDistinguisherArgs)
}

#[derive(Subcommand)]

enum Test {
    Vectors(TestVectorArgs),
}

fn main() -> io::Result<()> {
    SimpleLogger::new().init().unwrap();
    let args: Args = Args::parse();

    match args.action {
        Action::Transform(transform) => match transform{
            Transform::TruncateDifferential(args) => truncate_differential(args),
            Transform::SingleKey(args) => bin_single_key(args),
            Transform::Derive(args) => bin_derive(args)
        },
        Action::Evaluate(evaluate) => match evaluate {
            Evaluate::DifferentialDistinguisher(args) => evaluate_differential_distinguisher(args)
        },
        Action::Test(test) => match test {
            Test::Vectors(args) => bin_test_vectors(args)
        },
        Action::GenerateGraphviz(args) => bin_generate_graphviz(args),
        Action::Search(search) => {
            match search {
                Search::BestDifferentialCharacteristic(args) => bin_best_differential_characteristic(args),
                Search::BestTruncatedDifferentialCharacteristic(args) => bin_best_truncated_differential_characteristic(args),
            }
        }
    }
}