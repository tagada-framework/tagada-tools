use std::collections::{BTreeSet, HashMap};
use std::fs::File;
use std::io::{BufReader, BufWriter};
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle};
use log::info;
use serde::{Serialize, Deserialize};
use tagada_structs::common::{Domain, Value};
use tagada_structs::differential::{DifferentialFunction, DifferentialFunctionNode};
use tagada_structs::specification::Computable;
use tagada_structs::truncated_differential::TruncatedDifferentialFunction::{Linear, NonLinearTransition};
use tagada_structs::truncated_differential::TruncatedDifferentialFunctionNode;
use crate::abstraction::{abstract_domain};
use crate::DomainExtensions;

#[derive(Debug)]
pub struct OperatorInfoStore {
    abstraction_of: HashMap<DifferentialFunctionNode, TruncatedDifferentialFunctionNode>,
    changed: bool,
}

impl From<SerializedOperatorInfoStore> for OperatorInfoStore {
    fn from(mut value: SerializedOperatorInfoStore) -> Self {
        OperatorInfoStore {
            abstraction_of: value.abstraction_of.drain(..).collect(),
            changed: value.changed,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct SerializedOperatorInfoStore {
    abstraction_of: Vec<(DifferentialFunctionNode, TruncatedDifferentialFunctionNode)>,
    changed: bool,
}

impl From<&mut OperatorInfoStore> for SerializedOperatorInfoStore {
    fn from(value: &mut OperatorInfoStore) -> Self {
        SerializedOperatorInfoStore {
            abstraction_of: value.abstraction_of.iter().map(|(k, v)| (k.clone(), v.clone())).collect(),
            changed: value.changed,
        }
    }
}

fn cartesian_product<F>(domain: &Vec<Domain>, on_each: &mut F) where F: FnMut(&Vec<Value>) {
    let mut progress_bar = ProgressBar::with_draw_target(
        Some(domain.iter().fold(1, |acc, it| acc * it.len() as u64)),
        ProgressDrawTarget::stderr_with_hz(1),
    ).with_style(
        ProgressStyle::with_template("{wide_bar} {percent:>4.2} % ETA: {eta:7}")
            .unwrap()
    );
    cartesian_product_recursive(&domain.iter().map(|it| it.values()).collect(), 0, &mut vec![0; domain.len()], &mut progress_bar, on_each);
    progress_bar.finish();
    eprintln!();
}

fn cartesian_product_recursive<F>(domain: &Vec<Vec<usize>>, depth: usize, arr: &mut Vec<Value>, progress_bar: &mut ProgressBar, on_each: &mut F) where F: FnMut(&Vec<Value>) {
    if depth == domain.len() {
        on_each(arr)
    } else {
        for &value in &domain[depth] {
            arr[depth] = value;
            cartesian_product_recursive(domain, depth + 1, arr, progress_bar, on_each);
        }
        progress_bar.inc(domain[depth].len() as u64);
    }
}

fn compute_abstraction_of(differential_function_node: &DifferentialFunctionNode) -> TruncatedDifferentialFunctionNode {
    info!("Computing abstraction for {:?}", differential_function_node);
    info!("This could be long...");
    match &differential_function_node.function {
        DifferentialFunction::DDT { probabilities } => {
            let maximum_activation_probability = probabilities.iter().filter(|(&(d_in, d_out), _)| d_in != 0 && d_out != 0)
                .map(|(_, p)| p)
                .max()
                .cloned()
                .unwrap();

            TruncatedDifferentialFunctionNode {
                domain: differential_function_node.domain.iter().map(abstract_domain).collect(),
                co_domain: differential_function_node.co_domain.iter().map(abstract_domain).collect(),
                function: NonLinearTransition { maximum_activation_probability },
            }
        }
        DifferentialFunction::Linear { linear } => {
            let mut truth_table = BTreeSet::new();
            let mut outputs = vec![0; differential_function_node.co_domain.len()];
            cartesian_product(&differential_function_node.domain, &mut |inputs| {
                linear.call_into(inputs, &mut outputs);
                let mut truncated_tuple = Vec::with_capacity(inputs.len() + outputs.len());
                let mut truncated_inputs = inputs.iter().map(|it| *it > 0).collect();
                let mut truncated_outputs = outputs.iter().map(|it| *it > 0).collect();
                truncated_tuple.append(&mut truncated_inputs);
                truncated_tuple.append(&mut truncated_outputs);
                truth_table.insert(truncated_tuple);
            });
            let truth_table = truth_table.iter().cloned().collect::<Vec<_>>();
            TruncatedDifferentialFunctionNode {
                domain: differential_function_node.domain.iter().map(abstract_domain).collect(),
                co_domain: differential_function_node.co_domain.iter().map(abstract_domain).collect(),
                function: Linear { truth_table },
            }
        }
    }
}

impl OperatorInfoStore {
    const fn store_path() -> &'static str {
        "operator_info_store-1.0.0.mpk"
    }

    pub fn new() -> OperatorInfoStore {
        if let Ok(file) = File::open(OperatorInfoStore::store_path()) {
            let reader = BufReader::new(file);
            OperatorInfoStore::from(rmp_serde::from_read::<_, SerializedOperatorInfoStore>(reader).unwrap())
        } else {
            OperatorInfoStore {
                abstraction_of: HashMap::new(),
                changed: true,
            }
        }
    }

    pub fn abstraction_of(&mut self, function_node: &DifferentialFunctionNode) -> TruncatedDifferentialFunctionNode {
        let result = {
            let OperatorInfoStore { abstraction_of, changed } = self;
            abstraction_of
                .entry(function_node.clone())
                .or_insert_with_key(|f| {
                    *changed = true;
                    compute_abstraction_of(f)
                })
                .clone()
        };

        if self.changed {
            self.save();
        }

        result
    }

    fn save(&mut self) {
        self.changed = false;
        let bin_file = File::create(OperatorInfoStore::store_path()).unwrap();
        let mut writer = BufWriter::new(bin_file);
        rmp_serde::encode::write(&mut writer, &SerializedOperatorInfoStore::from(self)).unwrap();
    }
}

impl Drop for OperatorInfoStore {
    fn drop(&mut self) {
        if self.changed {
            self.save();
        }
    }
}