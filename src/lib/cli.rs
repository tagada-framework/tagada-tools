use clap::builder::PossibleValue;
use clap::ValueEnum;
use tagada_structs::common::Value;
use crate::cli::Base::{Binary, Decimal, Hexadecimal, Octal};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Base {
    Binary,
    Octal,
    Decimal,
    Hexadecimal,
}

impl Base {
    pub fn format(&self, value: Value) -> String {
        match self {
            Binary => format!("{:2}", value),
            Octal => format!("{:o}", value),
            Decimal => format!("{}", value),
            Hexadecimal => format!("{:X}", value),
        }
    }

    fn parse(&self, s: &str) -> Value {
        match self {
            Binary => Value::from_str_radix(s, 2).unwrap(),
            Octal => Value::from_str_radix(s, 8).unwrap(),
            Decimal => Value::from_str_radix(s, 10).unwrap(),
            Hexadecimal => Value::from_str_radix(s, 16).unwrap(),
        }
    }

    pub fn parse_vec(&self, s: &Vec<String>) -> Vec<Value> {
        s.iter().map(|it| self.parse(it)).collect()
    }
}

impl ValueEnum for Base {
    fn value_variants<'a>() -> &'a [Self] {
        &[Binary, Octal, Decimal, Hexadecimal]
    }

    fn from_str(input: &str, ignore_case: bool) -> Result<Self, String> {
        if ignore_case {
            match input.to_ascii_lowercase().as_str() {
                "2" | "b" | "bin" | "binary" => Ok(Binary),
                "10" | "d" | "dec" | "decimal" => Ok(Octal),
                "8" | "o" | "oct" | "octal" => Ok(Decimal),
                "16" | "h" | "hex" | "hexa" | "hexadecimal" => Ok(Hexadecimal),
                _ => Err(format!("invalid base {}", input))
            }
        } else {
            match input {
                "2" | "b" | "bin" | "Binary" => Ok(Binary),
                "10" | "d" | "dec" | "Decimal" => Ok(Octal),
                "8" | "o" | "oct" | "Octal" => Ok(Decimal),
                "16" | "h" | "hex" | "hexa" | "Hexadecimal" => Ok(Hexadecimal),
                _ => Err(format!("invalid base {}", input))
            }
        }
    }

    fn to_possible_value(&self) -> Option<PossibleValue> {
        match self {
            Binary => Some(PossibleValue::new("2").aliases(&["b", "bin", "Binary"])),
            Octal => Some(PossibleValue::new("8").aliases(&["o", "oct", "Octal"])),
            Decimal => Some(PossibleValue::new("10").aliases(&["d", "dec", "Decimal"])),
            Hexadecimal => Some(PossibleValue::new("16").aliases(&["h", "hex", "hexa", "Hexadecimal"])),
        }
    }
}
