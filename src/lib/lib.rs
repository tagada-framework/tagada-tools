extern crate core;

use tagada_structs::common::{Domain, Value};

pub mod operator_info_store;
pub mod abstraction;
pub mod dag_graphviz;
pub mod cli;
pub mod tools;
pub mod encrypt;

pub trait DomainExtensions {
    fn len(&self) -> usize;
    fn values(&self) -> Vec<Value>;
    fn contains(&self, value: &Value) -> bool;
}

impl DomainExtensions for Domain {
    fn len(&self) -> usize {
        match self {
            &Domain::Range { min, max } => max - min,
            Domain::Sparse { values } => values.len()
        }
    }

    fn values(&self) -> Vec<Value> {
        match self {
            &Domain::Range { min, max } => (min..max).collect(),
            Domain::Sparse { values } => values.iter().cloned().collect()
        }
    }

    fn contains(&self, value: &Value) -> bool {
        match self {
            &Domain::Range { min, max } => *value >= min && *value < max,
            Domain::Sparse { values } => values.contains(&value)
        }
    }
}


