use std::collections::BTreeSet;
use tagada_structs::common::{Domain, Value};

pub fn to_binary(v: Value) -> Value { v.min(1) }

pub fn abstract_values(values: &Vec<Value>) -> Vec<Value> {
    values.iter().map(|it| to_binary(*it)).collect()
}

pub fn abstract_domain(domain: &Domain) -> Domain {
    let values: Vec<Value> =
        match domain {
            &Domain::Range { min, max } => (min..max).collect(),
            Domain::Sparse { values } => values.iter().cloned().collect()
        };

    let mut abstract_values = BTreeSet::new();
    for value in values {
        abstract_values.insert(to_binary(value));
    }
    Domain::Sparse { values: abstract_values }
}
