use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use tagada_structs::TagadaGraph;
use crate::cli::Base;
use clap::Parser;
use itertools::Itertools;
use rand::{RngCore, SeedableRng};
use rand_xorshift::XorShiftRng;
use tagada_structs::common::{Domain, Value};
use tagada_structs::specification::{SpecificationGraph, StateNode, StateNodeUID};
use crate::encrypt::{propagate_encryption, topological_sort_transitions, Values};

#[derive(Parser)]
pub struct EvaluateDifferentialDistinguisherArgs {
    pub json_path: Option<PathBuf>,
    #[arg(short, long, required = true, use_value_delimiter = true)]
    pub key_values: Vec<String>,
    #[arg(short, long, required = true, use_value_delimiter = true)]
    pub plaintext_values: Vec<String>,
    #[arg(short, long, use_value_delimiter = true)]
    pub ciphertext_values: Vec<String>,
    #[arg(short, long, required = true)]
    pub base: Base,
    #[arg(short, long)]
    pub seed: Option<u64>,
    #[arg(short, long)]
    pub nb_keys: usize,
    #[arg(short, long)]
    pub nb_tries_per_key: usize,

}

pub fn evaluate_differential_distinguisher(args: EvaluateDifferentialDistinguisherArgs) -> io::Result<()> {
    let EvaluateDifferentialDistinguisherArgs {
        json_path,
        key_values,
        plaintext_values,
        base,
        ciphertext_values,
        seed,
        nb_keys,
        nb_tries_per_key
    } = args;

    let cipher: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    let cipher = match cipher {
        TagadaGraph::Specification { graph } => graph,
        _ => panic!("Differential distinguisher evaluation is only applicable to specification graphs"),
    };

    let key_differences = base.parse_vec(&key_values);
    let plaintext_differences = base.parse_vec(&plaintext_values);
    let ciphertext_differences = base.parse_vec(&ciphertext_values);

    evaluate_differential_distinguisher_(&cipher, &key_differences, &plaintext_differences, &ciphertext_differences, nb_keys, nb_tries_per_key, seed.unwrap_or(0));

    Ok(())
}

fn evaluate_differential_distinguisher_(
    cipher: &SpecificationGraph,
    key_differences: &Vec<Value>,
    plaintext_differences: &Vec<Value>,
    ciphertext_differences: &Vec<Value>,
    nb_keys: usize,
    nb_tries_per_key: usize,
    seed: u64,
) {
    let mut rand_gen = XorShiftRng::seed_from_u64(seed);

    let key_masks = cipher.key.iter()
        .map(|it| mask(&cipher.states[it.uid]))
        .collect::<Vec<_>>();
    let plaintext_masks = cipher.plaintext.iter()
        .map(|it| mask(&cipher.states[it.uid]))
        .collect::<Vec<_>>();

    let mut encryption0 = Values::new(cipher.states.len());
    let mut encryption1 = Values::new(cipher.states.len());

    // Records constant values
    for (uid, state) in cipher.states.iter().enumerate() {
        if let &StateNode::Constant { value } = state {
            encryption0.insert(StateNodeUID { uid }, value);
            encryption1.insert(StateNodeUID { uid }, value);
        }
    }

    fn generate_related(rand_gen: &mut XorShiftRng, states: &Vec<StateNodeUID>, masks: &Vec<Value>, differences: &Vec<Value>, e0: &mut Values, e1: &mut Values) {
        states.iter()
            .zip_eq(masks)
            .enumerate()
            .for_each(|(i, (&state_uid, &mask))| {
                let value = (rand_gen.next_u64() & mask as u64) as Value;
                e0.insert(state_uid, value);
                e1.insert(state_uid, value ^ differences[i])
            });
    }

    let sorted_transitions = topological_sort_transitions(cipher);
    for _ in 0..nb_keys {
        generate_related(&mut rand_gen, &cipher.key, &key_masks, &key_differences, &mut encryption0, &mut encryption1);
        let mut nb_valids = 0;
        for _ in 0..nb_tries_per_key {
            generate_related(&mut rand_gen, &cipher.plaintext, &plaintext_masks, &plaintext_differences, &mut encryption0, &mut encryption1);
            propagate_encryption(&cipher.functions, &sorted_transitions, &mut encryption0);
            propagate_encryption(&cipher.functions, &sorted_transitions, &mut encryption1);
            if cipher.ciphertext.iter().enumerate().all(|(i, it)| encryption0[it].unwrap() ^ encryption1[it].unwrap() == ciphertext_differences[i]) {
                nb_valids += 1;
            }
        }
        println!("2^{{{:?}}}", (nb_valids as f64 / nb_tries_per_key as f64).log2());
    }
}

fn mask(v: &StateNode) -> Value {
    match v {
        StateNode::Constant { value } => *value,
        StateNode::Variable { domain } => {
            match domain {
                &Domain::Range { min, max } => {
                    let mut mask = 0;
                    for i in min..max {
                        mask |= i;
                    }
                    mask
                }
                Domain::Sparse { values } => {
                    let mut mask = 0;
                    for &i in values.iter() {
                        mask |= i;
                    }
                    mask
                }
            }
        }
    }
}