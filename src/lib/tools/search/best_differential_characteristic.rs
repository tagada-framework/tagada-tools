use Ordering::{Less, Equal, Greater};
use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::fmt::{Debug, Display, Formatter};
use std::fs::{File};
use std::io;
use std::io::{BufReader};
use std::ops::Add;
use std::path::PathBuf;
use tagada_structs::differential::{Hundredth, MinusLog2Probability};
use tagada_structs::TagadaGraph;
use tagada_structs::truncated_differential::TruncatedDifferentialGraph;

use clap::Parser;
use log::info;
use measure_time::info_time;
use quine_mc_cluskey::Bool;
use tagada_structs::common::Value;
use NullableProbability::{StrictlyPositive, Zero};
use crate::tools::Assignment;
use crate::tools::backends::FznSolver;
use crate::tools::backends::gurobi::Gurobi;
use crate::tools::backends::minizinc::MiniZinc;
use crate::tools::backends::or_tools::OrTools;
use crate::tools::backends::picat::Picat;
use crate::tools::backends::tagada_step2::TagadaStep2;
use crate::tools::mzn_builder::Encoding;
use crate::tools::search::best_truncated_differential_characteristic::{best_truncated_differential_characteristic, next_theoretical_ub, next_truncated_differential_characteristic, Solver};

#[derive(Parser)]
#[command(about = "Search for the differential characteristic with the highest probability.", long_about = None)]
pub struct BestDifferentialCharacteristicArgs {
    pub diff_json_path: PathBuf,
    pub truncated_json_path: PathBuf,
    #[command(subcommand)]
    pub solver: Solver,
    #[arg(short, long)]
    pub minizinc_path: Option<PathBuf>,
    #[arg(short, long, default_value = "cnf")]
    pub encoding: Encoding,
    #[arg(short, long)]
    pub jdk: Option<PathBuf>,
    #[arg(short, long)]
    pub tagada_step2: Option<PathBuf>,
}

pub fn bin_best_differential_characteristic(args: BestDifferentialCharacteristicArgs) -> io::Result<()> {
    info_time!("picat solving");
    let BestDifferentialCharacteristicArgs {
        diff_json_path,
        truncated_json_path,
        minizinc_path,
        solver,
        jdk,
        tagada_step2,
        encoding
    } = args;
    let minizinc = MiniZinc::new(&minizinc_path);
    let solver: Box<dyn FznSolver> = match solver {
        Solver::Picat { picat_path, fzn_picat_sat } => Box::new(Picat::new(&picat_path, &fzn_picat_sat)),
        Solver::OrTools { or_tools_path } => Box::new(OrTools::new(&or_tools_path)),
        Solver::Gurobi { dll_path, nb_threads } => Box::new(Gurobi::new(&minizinc, dll_path, nb_threads))
    };
    let tagada_step2 = TagadaStep2::new(&jdk, &tagada_step2);
    let file = File::open(&diff_json_path)?;
    let reader = BufReader::new(file);
    let differential_graph: TagadaGraph = serde_json::from_reader(reader)?;
    match differential_graph {
        TagadaGraph::Differential { .. } => {}
        _ => panic!("only differential graphs are accepted as first argument")
    };
    let file = File::open(truncated_json_path)?;
    let reader = BufReader::new(file);
    let truncated_differential_graph: TagadaGraph = serde_json::from_reader(reader)?;
    let truncated_differential_graph = match truncated_differential_graph {
        TagadaGraph::TruncatedDifferential { graph } => graph,
        _ => panic!("only truncated differential graphs are accepted as second argument")
    };
    println!("{}", serde_json::to_string(&
        search_best_differential_characteristic(
            &minizinc,
            &solver,
            &tagada_step2,
            &diff_json_path,
            &truncated_differential_graph,
            encoding
        )
    )?);
    Ok(())
}

fn search_best_differential_characteristic(
    mzn: &MiniZinc,
    solver: &Box<dyn FznSolver>,
    tagada_step2: &TagadaStep2,
    differential_graph: &PathBuf,
    truncated_differential_graph: &TruncatedDifferentialGraph,
    encoding: Encoding
) -> Option<Assignment> {
    let mut best_differential_characteristic = None;
    let mut lb = Zero;
    let mut ub = NullableProbability::ONE;
    let mut seen = Vec::new();
    let mut dnf = BTreeMap::<Vec<Vec<bool>>, Bool>::new();
    let mut cnf = BTreeMap::<Vec<Vec<bool>>, Bool>::new();
    let mut table = BTreeMap::<Vec<Vec<bool>>, String>::new();
    let mut s1 = best_truncated_differential_characteristic(&mzn, solver, truncated_differential_graph, &seen, ub, encoding, &mut dnf, &mut cnf, &mut table);
    ub = probability(&s1);
    info!("LB: {} UB: {}", lb, ub);
    while lb < ub {
        let mut checked_s1 = s1.clone().unwrap();
        checked_s1.retain(|name, _| name.starts_with("in_"));
        let s2 = tagada_step2.bounded_optimal_differential_characteristic(differential_graph, &checked_s1, lb);
        seen.push(checked_s1);
        let p_s2 = probability(&s2);
        if lb < p_s2 {
            lb = p_s2;
            best_differential_characteristic = s2;
            info!("LB: {} UB: {}", lb, ub);
        }
        if lb < ub {
            s1 = next_truncated_differential_characteristic(&mzn, solver, &truncated_differential_graph, &seen, ub, encoding, &mut dnf, &mut cnf, &mut table);
            if s1.is_none() {
                ub = next_theoretical_ub(truncated_differential_graph, ub);
                info!("LB: {} UB: {}", lb, ub);
                if lb >= ub {
                    return best_differential_characteristic;
                }
                seen.clear();
                s1 = best_truncated_differential_characteristic(&mzn, solver, &truncated_differential_graph, &seen, ub, encoding, &mut dnf, &mut cnf, &mut table);
            }
            ub = probability(&s1);
            info!("LB: {} UB: {}", lb, ub);
        }
    }
    best_differential_characteristic
}

fn probability(assignment: &Option<Assignment>) -> NullableProbability {
    if let Some(assignment) = assignment {
        let minus_log2_value: Value = assignment[&String::from("obj")];
        StrictlyPositive(MinusLog2Probability(Hundredth(minus_log2_value)))
    } else {
        Zero
    }
}

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum NullableProbability {
    StrictlyPositive(MinusLog2Probability),
    Zero,
}

impl NullableProbability {
    pub const ONE: NullableProbability = StrictlyPositive(MinusLog2Probability(Hundredth(0)));
}

impl Add<MinusLog2Probability> for NullableProbability {
    type Output = NullableProbability;

    fn add(self, rhs: MinusLog2Probability) -> Self::Output {
        match self {
            Zero => StrictlyPositive(rhs),
            StrictlyPositive(this) => StrictlyPositive(MinusLog2Probability(Hundredth(this.0.0 + rhs.0.0)))
        }
    }
}

impl Display for NullableProbability {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            StrictlyPositive(MinusLog2Probability(Hundredth(p))) => f.write_fmt(format_args!("2^{{-{:.2}}}", *p as f64 / 100.0)),
            Zero => f.write_str("0")
        }
    }
}

impl NullableProbability {
    pub fn as_step2_cli_arg(&self) -> String {
        match self {
            StrictlyPositive(MinusLog2Probability(Hundredth(value))) => value.to_string(),
            Zero => String::from("null")
        }
    }
}

impl Ord for NullableProbability {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Zero, Zero) => Equal,
            (Zero, StrictlyPositive(_)) => Less,
            (StrictlyPositive(_), Zero) => Greater,
            (StrictlyPositive(lhs), StrictlyPositive(rhs)) => lhs.cmp(&rhs),
        }
    }
}

impl PartialOrd for NullableProbability {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}