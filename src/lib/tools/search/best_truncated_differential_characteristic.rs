use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::fs::File;
use std::{io};
use std::io::{BufReader, BufWriter, Write};
use std::path::{PathBuf};
use clap::Parser;
use tagada_structs::differential::{Hundredth, MinusLog2Probability};
use tagada_structs::TagadaGraph;
use tagada_structs::truncated_differential::{TruncatedDifferentialFunction, TruncatedDifferentialGraph, TruncatedDifferentialStateNode, TruncatedDifferentialStateNodeUID, TruncatedDifferentialTransition};
use TruncatedDifferentialFunction::{Linear, NonLinearTransition};
use TruncatedDifferentialStateNode::{Constant, Variable};
use NullableProbability::Zero;
use crate::tools::{Assignment, tempfile_with_extension};
use crate::tools::backends::minizinc::MiniZinc;
use crate::tools::backends::or_tools::OrTools;
use crate::tools::backends::FznSolver;
use crate::tools::backends::picat::Picat;
use crate::tools::mzn_builder::{Encoding, eq_expr, ge_expr, MiniZincBuilder, paren, sum_expr, var_name};
use clap::{Subcommand};
use disjoint_sets::UnionFind;
use quine_mc_cluskey::Bool;
use crate::tools::backends::gurobi::Gurobi;
use crate::tools::search::best_differential_characteristic::NullableProbability;
use crate::tools::search::best_differential_characteristic::NullableProbability::StrictlyPositive;

#[derive(Parser)]
#[command(about = "Search for the truncated differential characteristic with the highest probability.", long_about = None)]
pub struct BestTruncatedDifferentialCharacteristicArgs {
    pub truncated_json_path: PathBuf,
    pub seen_json_path: Option<PathBuf>,
    #[command(subcommand)]
    pub solver: Solver,
    #[arg(short, long)]
    pub minizinc_path: Option<PathBuf>,
    #[arg(short, long, default_value = "cnf")]
    pub encoding: Encoding,
}

#[derive(Subcommand)]
pub enum Solver {
    Picat {
        #[arg(short, long)]
        picat_path: Option<PathBuf>,
        #[arg(short, long)]
        fzn_picat_sat: Option<PathBuf>,
    },
    OrTools {
        #[arg(short, long)]
        or_tools_path: Option<PathBuf>,
    },
    Gurobi {
        #[arg(short, long)]
        dll_path: Option<PathBuf>,
        #[arg(long)]
        nb_threads: Option<usize>
    }
}

fn is_sbox(f: &TruncatedDifferentialFunction) -> bool {
    if let NonLinearTransition { .. } = f {
        true
    } else {
        false
    }
}

fn merge_sbox_inputs_and_outputs(g: &TruncatedDifferentialGraph) -> UnionFind<usize> {
    let mut union_set = UnionFind::new(g.states.len());
    for TruncatedDifferentialTransition { inputs, function, outputs } in &g.transitions {
        if is_sbox(&g.functions[function.uid].function) {
            if inputs.len() == 1 && outputs.len() == 1 {
                if !union_set.equiv(inputs[0].uid, outputs[0].uid) {
                    union_set.union(inputs[0].uid, outputs[0].uid);
                }
            }
        }
    }
    union_set
}

pub fn bin_best_truncated_differential_characteristic(args: BestTruncatedDifferentialCharacteristicArgs) -> io::Result<()> {
    let BestTruncatedDifferentialCharacteristicArgs {
        truncated_json_path,
        seen_json_path,
        minizinc_path,
        solver,
        encoding
    } = args;

    let minizinc = MiniZinc::new(&minizinc_path);
    let solver: Box<dyn FznSolver> = match solver {
        Solver::Picat { picat_path, fzn_picat_sat } => Box::new(Picat::new(&picat_path, &fzn_picat_sat)),
        Solver::OrTools { or_tools_path } => Box::new(OrTools::new(&or_tools_path)),
        Solver::Gurobi { dll_path, nb_threads } => Box::new(Gurobi::new(&minizinc, dll_path, nb_threads))
    };

    let file = File::open(&truncated_json_path)?;
    let reader = BufReader::new(file);
    let differential_graph: TagadaGraph = serde_json::from_reader(reader)?;
    let graph = match differential_graph {
        TagadaGraph::TruncatedDifferential { graph } => graph,
        _ => panic!("only truncated differential graphs are accepted as first argument")
    };

    let seen: Vec<Assignment> = if let Some(seen) = seen_json_path {
        let file = File::open(&seen)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        vec![]
    };

    let mut dnf = BTreeMap::<Vec<Vec<bool>>, Bool>::new();
    let mut cnf = BTreeMap::<Vec<Vec<bool>>, Bool>::new();
    let mut table = BTreeMap::<Vec<Vec<bool>>, String>::new();
    let result = best_truncated_differential_characteristic(
        &minizinc,
        &solver,
        &graph,
        &seen,
        StrictlyPositive(MinusLog2Probability(Hundredth(0))),
        encoding,
        &mut dnf,
        &mut cnf,
        &mut table
    );
    println!("{}", serde_json::to_string(&result)?);
    Ok(())
}

pub fn best_truncated_differential_characteristic(
    minizinc: &MiniZinc,
    fzn_solver: &Box<dyn FznSolver>,
    graph: &TruncatedDifferentialGraph,
    seen: &Vec<Assignment>,
    ub: NullableProbability,
    encoding: Encoding,
    dnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &mut BTreeMap<Vec<Vec<bool>>, String>,
) -> Option<Assignment> {
    let (mzn, file) = tempfile_with_extension("mzn")
        .unwrap();
    {
        let mut writer = BufWriter::new(file);
        writer.write(truncated_differential_graph_to_opt_model(&graph, seen, ub, encoding, dnf, cnf, table).as_bytes())
            .unwrap();
    }
    let (fzn, ozn) = minizinc.compile_for(&mzn, &fzn_solver);
    let (_, output) = fzn_solver.solve(&fzn);
    minizinc.format(output, &ozn)
}

pub fn next_truncated_differential_characteristic(
    minizinc: &MiniZinc,
    fzn_solver: &Box<dyn FznSolver>,
    graph: &TruncatedDifferentialGraph,
    seen: &Vec<Assignment>,
    ub: NullableProbability,
    encoding: Encoding,
    dnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &mut BTreeMap<Vec<Vec<bool>>, String>,
) -> Option<Assignment> {
    let (mzn, file) = tempfile_with_extension("mzn")
        .unwrap();
    {
        let mut writer = BufWriter::new(file);
        writer.write(truncated_differential_graph_to_enum_model(graph, seen, ub, encoding, dnf, cnf, table).as_bytes())
            .unwrap();
    }
    let (fzn, ozn) = minizinc.compile_for(&mzn, &fzn_solver);
    let (_, output) = fzn_solver.solve(&fzn);
    minizinc.format(output, &ozn)
}

fn truncated_differential_graph_to_model<'t>(
    graph: &TruncatedDifferentialGraph,
    seen: &Vec<Assignment>,
    encoding: Encoding,
    dnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &'t mut BTreeMap<Vec<Vec<bool>>, String>,
) -> (MiniZincBuilder<'t>, BTreeMap<MinusLog2Probability, Vec<TruncatedDifferentialStateNodeUID>>, Vec<String>) {
    let union_find = merge_sbox_inputs_and_outputs(graph);
    let repr = |v: &TruncatedDifferentialStateNodeUID| -> TruncatedDifferentialStateNodeUID {
        TruncatedDifferentialStateNodeUID { uid: union_find.find(v.uid) }
    };

    let mut mzn_model = MiniZincBuilder::new(dnf, cnf, table);
    let mut objective = BTreeMap::<MinusLog2Probability, Vec<TruncatedDifferentialStateNodeUID>>::new();

    for (uid, state) in graph.states.iter().enumerate() {
        let uid = TruncatedDifferentialStateNodeUID { uid };
        match state {
            Variable { domain } => mzn_model.create_variable(&uid, &domain),
            Constant { value } => mzn_model.create_constant(&uid, &value)
        };
    }

    let mut sbox_inputs = Vec::new();
    for TruncatedDifferentialTransition { inputs, function, outputs } in graph.transitions.iter() {
        let function = &graph.functions[function.uid];
        match &function.function {
            &NonLinearTransition { maximum_activation_probability } => {
                let vars_for_probability = objective.entry(maximum_activation_probability).or_insert_with(|| Vec::new());
                vars_for_probability.push(repr(&inputs[0]));
                mzn_model.comment("post s-box");
                mzn_model.equal_to(
                    var_name(&inputs[0]),
                    var_name(&outputs[0]),
                );
                sbox_inputs.push(var_name(&repr(&inputs[0])));
            }
            Linear { truth_table } => {
                let mut scope = Vec::with_capacity(inputs.len() + outputs.len());
                scope.extend(inputs.iter().map(repr));
                scope.extend(outputs.iter().map(repr));
                mzn_model.table(&scope, truth_table, encoding);
            }
        }
    }
    let cipher_inputs = graph.plaintext.iter().chain(&graph.key)
        .map(repr)
        .collect::<Vec<_>>();
    mzn_model.comment("at least one input must be positive");
    mzn_model.greater_than(paren(sum_expr(cipher_inputs.iter())), 0);

    for (i, solution) in seen.iter().enumerate() {
        mzn_model.comment(format!("forbid solution {}", i + 1));
        mzn_model.forbid_sbox_assignment(solution, &graph.names);
    }

    mzn_model.comment("configure output format");
    mzn_model.configure_output(&graph.names);

    (mzn_model, objective, sbox_inputs)
}

pub fn next_theoretical_ub(graph: &TruncatedDifferentialGraph, ub: NullableProbability) -> NullableProbability {
    if ub == Zero { return Zero; }
    let mut queue = VecDeque::new();
    queue.push_back(StrictlyPositive(MinusLog2Probability(Hundredth(0))));
    let mut activation_values = BTreeSet::new();
    for function in graph.functions.iter() {
        match function.function {
            NonLinearTransition { maximum_activation_probability } => {
                activation_values.insert(maximum_activation_probability);
            }
            _ => {}
        }
    }
    loop {
        let current_ub = queue.pop_front().unwrap();
        if current_ub < ub {
            return current_ub;
        }
        for &activation_value in activation_values.iter() {
            let new_ub = current_ub + activation_value;
            if !queue.contains(&new_ub) && new_ub != current_ub {
                queue.push_back(new_ub);
            }
        }
    }
}

fn truncated_differential_graph_to_opt_model(
    graph: &TruncatedDifferentialGraph,
    seen: &Vec<Assignment>,
    ub: NullableProbability,
    encoding: Encoding,
    dnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &mut BTreeMap<Vec<Vec<bool>>, String>,
) -> String {
    let (mut mzn_model, objective, sbox_inputs) = truncated_differential_graph_to_model(graph, seen, encoding, dnf, cnf, table);
    mzn_model.comment("objective function");
    let obj = mzn_model.minimize_weighted_sum(&objective, &sbox_inputs);
    if let StrictlyPositive(p) = ub {
        mzn_model.constraint(ge_expr(obj, p.0.0));
    }
    mzn_model.build()
}

fn truncated_differential_graph_to_enum_model(
    graph: &TruncatedDifferentialGraph,
    seen: &Vec<Assignment>,
    ub: NullableProbability,
    encoding: Encoding,
    dnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &mut BTreeMap<Vec<Vec<bool>>, String>,
) -> String {
    let (mut mzn_model, objective, sbox_inputs) = truncated_differential_graph_to_model(graph, seen, encoding, dnf, cnf, table);
    mzn_model.comment("objective function");
    let obj = mzn_model.satisfy_weighted_sum(&objective, &sbox_inputs);
    let ub = match ub {
        StrictlyPositive(MinusLog2Probability(Hundredth(p))) => p,
        Zero => panic!("expects a non null probability")
    };
    mzn_model.constraint(eq_expr(obj, ub));
    mzn_model.build()
}