use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub struct Diff<T>(pub T, pub T);

impl<T: Copy> Copy for Diff<T> {}

impl<T: Clone> Clone for Diff<T> {
    fn clone(&self) -> Self {
        Diff(self.0.clone(), self.1.clone())
    }
}

impl<T: Eq> Eq for Diff<T> {}

impl<T: PartialEq> PartialEq for Diff<T> {
    fn eq(&self, other: &Self) -> bool {
        (self.0 == other.0 && self.1 == other.1) ||
            (self.1 == other.0 && self.0 == other.1)
    }
}

impl<T: Hash> Hash for Diff<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let mut hash_a = DefaultHasher::new();
        self.0.hash(&mut hash_a);
        let mut hash_b = DefaultHasher::new();
        self.1.hash(&mut hash_b);
        state.write_u64(hash_a.finish() ^ hash_b.finish());
    }
}