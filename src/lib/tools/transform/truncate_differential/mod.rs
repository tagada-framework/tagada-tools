use std::collections::{BTreeSet, HashMap, HashSet};
use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use clap::{Args, Parser};
use log::info;
use tagada_structs::common::Version;
use tagada_structs::common::Domain::Range;
use tagada_structs::differential::{DifferentialFunction, DifferentialFunctionNode, DifferentialGraph, DifferentialStateNode, DifferentialStateNodeUID, DifferentialTransition};
use tagada_structs::specification::LinearFunction::{BitwiseXOR, Equal};
use tagada_structs::TagadaGraph;
use tagada_structs::truncated_differential::{TruncatedDifferentialFunctionNode, TruncatedDifferentialFunctionNodeUID, TruncatedDifferentialGraph, TruncatedDifferentialStateNode, TruncatedDifferentialStateNodeUID, TruncatedDifferentialTransition};
use crate::abstraction::abstract_domain;
use crate::operator_info_store::OperatorInfoStore;
use crate::tools::transform::truncate_differential::diff::Diff;
use crate::tools::transform::truncate_differential::diff_handler::DiffHandler;

mod diff;
mod diff_handler;

#[derive(Parser)]
#[command(about = "Transform a differential graph into a truncated differential graph.", long_about = None)]
pub struct TruncateDifferentialArgs {
    pub json_path: Option<PathBuf>,
    #[clap(flatten)]
    features: Features,
}

#[derive(Args, Copy, Clone, Eq, PartialEq)]
struct Features {
    #[arg(short, long, default_value_t = 0)]
    generate_xors: usize,
    #[arg(short, long)]
    diff_variables: bool,
    #[arg(short, long, requires = "diff_variables")]
    transitivity: bool,
}

pub fn truncate_differential(args: TruncateDifferentialArgs) -> io::Result<()> {
    let TruncateDifferentialArgs { json_path, features } = args;
    let graph: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    let graph = match graph {
        TagadaGraph::Differential { graph } => truncate(graph, features),
        _ => panic!("truncation is only applicable to differential graphs")
    };

    println!("{}", serde_json::ser::to_string(&TagadaGraph::TruncatedDifferential { graph })?);
    Ok(())
}

fn truncate(graph: DifferentialGraph, features: Features) -> TruncatedDifferentialGraph {
    let mut builder = TruncatedDifferentialGraphBuilder::new();

    let DifferentialGraph {
        mut states,
        functions,
        transitions,
        plaintext,
        key,
        ciphertext,
        names,
        blocks,
        ..
    } = graph;

    let mut xor_equations = Equations::new();
    for DifferentialTransition { inputs, function, outputs } in &transitions {
        let truncated_differential_function_uid = builder.truncate_function_node(&functions[function.uid]);
        if let DifferentialFunction::Linear { linear: BitwiseXOR } = &functions[function.uid].function {
            let mut equation = Equation::new();
            for input in inputs {
                equation.insert(input.uid);
            }
            for output in outputs {
                equation.insert(output.uid);
            }
            xor_equations.insert(equation);
        }
        builder.create_transition(&states, &inputs, truncated_differential_function_uid, outputs);
    }

    let xor_eql = if features.generate_xors > 0 {
        let new_xors = combine(&xor_equations, &xor_equations, features.generate_xors);
        info!("generated xor equations: {}", new_xors.len());
        for xor in new_xors.iter() {
            let domains = xor.iter()
                .map(|&uid| {
                    let node = &states[builder.states[&DifferentialStateNodeUID { uid }].uid];
                    match node {
                        DifferentialStateNode::Variable { domain } => domain.clone(),
                        &DifferentialStateNode::Constant { value } => Range { min: value, max: value + 1 },
                        DifferentialStateNode::Probability { .. } => panic!("probability variables shouldn’t be present in the graph"),
                    }
                })
                .collect::<Vec<_>>();
            let variables = xor.iter()
                .map(|&uid| DifferentialStateNodeUID { uid })
                .collect::<Vec<_>>();
            if domains.len() > 2 {
                let (co_domain, domain) = domains.split_last().unwrap();
                let truncated_differential_function_uid = builder.truncate_function_node(&DifferentialFunctionNode {
                    domain: Vec::from(domain),
                    co_domain: vec![co_domain.clone()],
                    function: DifferentialFunction::Linear { linear: BitwiseXOR },
                });
                let (output, inputs) = variables
                    .split_last()
                    .unwrap();

                builder.create_transition(
                    &states,
                    &Vec::from(inputs),
                    truncated_differential_function_uid,
                    &vec![output.clone()],
                );
            } else if domains.len() == 2 {
                let equality = builder.truncated_equality();
                builder.create_transition(
                    &states,
                    &vec![variables[0]],
                    equality,
                    &vec![variables[1]]
                );
            } else {
                // TODO variables[0] = 0
            }
        }
        new_xors
    } else {
        Equations::new()
    };
    let xor_eql = union(&xor_eql, &xor_equations);

    if features.diff_variables {
        let mut diff_handler = DiffHandler::new();

        for xor_eq in xor_eql.iter() {
            if xor_eq.len() == 3 {
                let mut iter = xor_eq.iter().map(|it| DifferentialStateNodeUID { uid: *it });
                let a = iter.next().unwrap();
                let b = iter.next().unwrap();
                let c = iter.next().unwrap();
                diff_handler.set(Diff(a, b), c);
                diff_handler.set(Diff(a, c), b);
                diff_handler.set(Diff(b, c), a);
            }
        }

        for xor_eq in xor_eql.iter() {
            if xor_eq.len() == 4 {
                let mut iter = xor_eq.iter().map(|it| DifferentialStateNodeUID { uid: *it });
                let a = iter.next().unwrap();
                let b = iter.next().unwrap();
                let c = iter.next().unwrap();
                let d = iter.next().unwrap();
                let diff_ab = diff_handler.get(&mut builder, &mut states, a, b);
                let diff_ac = diff_handler.get(&mut builder, &mut states, a, c);
                let diff_ad = diff_handler.get(&mut builder, &mut states, a, d);
                let diff_bc = diff_handler.get(&mut builder, &mut states, b, c);
                let diff_bd = diff_handler.get(&mut builder, &mut states, b, d);
                let diff_cd = diff_handler.get(&mut builder, &mut states, c, d);
                let equality = builder.truncated_equality();
                builder.create_transition(&states, &vec![diff_ab], equality, &vec![diff_cd]);
                builder.create_transition(&states, &vec![diff_ac], equality, &vec![diff_bd]);
                builder.create_transition(&states, &vec![diff_ad], equality, &vec![diff_bc]);
            }
        }

        for i in 0..transitions.len() {
            let transition_i = &transitions[i];
            for j in (i + 1)..transitions.len() {
                let transition_j = &transitions[j];
                if transition_i.function == transition_j.function {
                    let function = &functions[transition_i.function.uid];
                    if let DifferentialFunction::Linear { linear: inner_function } = &function.function {
                        if &BitwiseXOR == inner_function { continue; };

                        let DifferentialTransition {
                            inputs: xi,
                            outputs: yi,
                            ..
                        } = transition_i;

                        let DifferentialTransition {
                            inputs: xj,
                            outputs: yj,
                            ..
                        } = transition_j;

                        let mut diff_inputs = Vec::with_capacity(function.domain.len());
                        for (v, _dom) in function.domain.iter().enumerate() {
                            diff_inputs.push(diff_handler.get(&mut builder, &mut states, xi[v], xj[v]));
                        }
                        let mut diff_outputs = Vec::with_capacity(function.co_domain.len());
                        for (v, _dom) in function.co_domain.iter().enumerate() {
                            diff_outputs.push(diff_handler.get(&mut builder, &mut states, yi[v], yj[v]));
                        }
                        let truncated_function = builder.truncate_function_node(function);
                        builder.create_transition(
                            &states,
                            &diff_inputs,
                            truncated_function,
                            &diff_outputs,
                        );
                    }
                }
            }
        }

        if features.transitivity {
            let diff_vars = diff_handler.finish();
            let abstract_transitivity_table = builder.truncated_xor();
            for (&Diff(a, b), &diff1) in diff_vars.iter() {
                for (&Diff(c, d), &diff2) in diff_vars.iter() {
                    if a == c {
                        if let Some(&diff3) = diff_vars.get(&Diff(b, d)) {
                            builder.create_transition(
                                &states,
                                &vec![diff1, diff2],
                                abstract_transitivity_table,
                                &vec![diff3],
                            );
                        }
                    }
                    if a == d {
                        if let Some(&diff3) = diff_vars.get(&Diff(b, c)) {
                            builder.create_transition(
                                &states,
                                &vec![diff1, diff2],
                                abstract_transitivity_table,
                                &vec![diff3],
                            );
                        }
                    }
                }
            }
        }
    }
    builder.redirect_plaintext(&plaintext);
    builder.redirect_key(&key);
    builder.redirect_ciphertext(&ciphertext);
    builder.redirect_names(&names);
    builder.redirect_blocks(&blocks);
    builder.build()
}

pub(crate) struct TruncatedDifferentialGraphBuilder {
    graph: TruncatedDifferentialGraph,
    operator_info_store: OperatorInfoStore,
    cache: HashMap<TruncatedDifferentialFunctionNode, TruncatedDifferentialFunctionNodeUID>,
    states: HashMap<DifferentialStateNodeUID, TruncatedDifferentialStateNodeUID>,
}

impl TruncatedDifferentialGraphBuilder {
    fn truncated_equality(&mut self) -> TruncatedDifferentialFunctionNodeUID {
        self.truncate_function_node(&DifferentialFunctionNode {
            domain: vec![Range { min: 0, max: 2 }],
            co_domain: vec![Range { min: 0, max: 2 }],
            function: DifferentialFunction::Linear { linear: Equal },
        })
    }

    fn truncated_xor(&mut self) -> TruncatedDifferentialFunctionNodeUID {
        self.truncate_function_node(&DifferentialFunctionNode {
            domain: vec![Range { min: 0, max: 3 }, Range { min: 0, max: 3 }],
            co_domain: vec![Range { min: 0, max: 3 }],
            function: DifferentialFunction::Linear { linear: BitwiseXOR },
        })
    }

    fn new() -> TruncatedDifferentialGraphBuilder {
        TruncatedDifferentialGraphBuilder {
            graph: TruncatedDifferentialGraph {
                version: Version(0, 10, 0),
                states: Vec::new(),
                functions: Vec::new(),
                transitions: Vec::new(),
                plaintext: Vec::new(),
                key: Vec::new(),
                ciphertext: Vec::new(),
                names: HashMap::new(),
                blocks: HashMap::new(),
            },
            operator_info_store: OperatorInfoStore::new(),
            cache: HashMap::new(),
            states: HashMap::new(),
        }
    }

    pub(crate) fn create_transition(
        &mut self,
        states: &Vec<DifferentialStateNode>,
        inputs: &Vec<DifferentialStateNodeUID>,
        function: TruncatedDifferentialFunctionNodeUID,
        outputs: &Vec<DifferentialStateNodeUID>,
    ) {
        let truncated_differential_inputs: Vec<TruncatedDifferentialStateNodeUID> = inputs.iter()
            .map(|it| self.get_or_create_state(it, &states[it.uid]))
            .collect();
        assert_eq!(truncated_differential_inputs.len(), self.graph.functions[function.uid].domain.len());
        let truncated_differential_outputs: Vec<TruncatedDifferentialStateNodeUID> = outputs.iter()
            .map(|it| self.get_or_create_state(it, &states[it.uid]))
            .collect();
        assert_eq!(truncated_differential_outputs.len(), self.graph.functions[function.uid].co_domain.len());

        self.graph.transitions.push(TruncatedDifferentialTransition {
            inputs: truncated_differential_inputs,
            function,
            outputs: truncated_differential_outputs,
        });
    }

    pub(crate) fn redirect_plaintext(&mut self, plaintext: &Vec<DifferentialStateNodeUID>) {
        self.graph.plaintext = plaintext.iter()
            .map(|it| self.states[it])
            .collect();
    }

    pub(crate) fn redirect_key(&mut self, key: &Vec<DifferentialStateNodeUID>) {
        self.graph.key = key.iter()
            .map(|it| self.states[it])
            .collect();
    }

    pub(crate) fn redirect_ciphertext(&mut self, ciphertext: &Vec<DifferentialStateNodeUID>) {
        self.graph.ciphertext = ciphertext.iter()
            .map(|it| self.states[it])
            .collect();
    }

    pub(crate) fn redirect_names(&mut self, names: &HashMap<String, DifferentialStateNodeUID>) {
        for (name, state_node_uid) in names.iter() {
            self.redirect_name(name, state_node_uid);
        }
    }

    fn redirect_name(&mut self, name: &String, uid: &DifferentialStateNodeUID) {
        if let Some(truncated_differential_state_node) = self.states.get(uid) {
            self.graph.names.insert(name.clone(), *truncated_differential_state_node);
        }
    }

    pub fn redirect_blocks(&mut self, blocks: &HashMap<String, Vec<DifferentialStateNodeUID>>) {
        for (name, block) in blocks.iter() {
            self.redirect_block(name, block);
        }
    }

    fn redirect_block(&mut self, name: &String, block: &Vec<DifferentialStateNodeUID>) {
        let block = block.iter()
            .filter_map(|it| self.states.get(it))
            .cloned()
            .collect::<Vec<_>>();
        if !block.is_empty() {
            self.graph.blocks.insert(name.clone(), block);
        }
    }

    pub(crate) fn truncate_function_node(&mut self, diff_function: &DifferentialFunctionNode) -> TruncatedDifferentialFunctionNodeUID {
        let truncated_function = self.operator_info_store.abstraction_of(diff_function);
        let TruncatedDifferentialGraphBuilder {
            graph,
            cache,
            ..
        } = self;
        *cache.entry(truncated_function).or_insert_with_key(|func| {
            let differential_function_node_uid = TruncatedDifferentialFunctionNodeUID { uid: graph.functions.len() };
            graph.functions.push(func.clone());
            differential_function_node_uid
        })
    }

    pub(crate) fn get_or_create_state(&mut self, uid: &DifferentialStateNodeUID, state_node: &DifferentialStateNode) -> TruncatedDifferentialStateNodeUID {
        let TruncatedDifferentialGraphBuilder {
            graph,
            states,
            ..
        } = self;

        *states.entry(*uid).or_insert_with(|| {
            let differential_state_node_uid = TruncatedDifferentialStateNodeUID { uid: graph.states.len() };
            let differential_state_node = match state_node {
                DifferentialStateNode::Variable { domain } => TruncatedDifferentialStateNode::Variable { domain: abstract_domain(domain) },
                &DifferentialStateNode::Constant { value } => TruncatedDifferentialStateNode::Constant { value },
                _ => unimplemented!("probabilities must not appear in the graph"),
            };
            graph.states.push(differential_state_node);
            differential_state_node_uid
        })
    }

    fn build(self) -> TruncatedDifferentialGraph {
        self.graph
    }
}

type Equation = BTreeSet<usize>;
type Equations = HashSet<Equation>;

fn combine(lhs: &Equations, rhs: &Equations, max_len: usize) -> Equations {
    if lhs.is_empty() { return Equations::new(); }

    let mut new_equations_set = Equations::new();
    for equation_1 in lhs {
        for equation_2 in rhs {
            if equation_1 != equation_2 {
                let result = xor(equation_1, equation_2);
                if result.len() < (equation_1.len() + equation_2.len()).min(max_len) && !rhs.contains(&result) {
                    new_equations_set.insert(result);
                }
            }
        }
    }
    union(&new_equations_set, &combine(&new_equations_set, &(union(&new_equations_set, &rhs)), max_len))
}

fn union(lhs: &Equations, rhs: &Equations) -> Equations {
    lhs.union(rhs).cloned().collect()
}

fn xor(lhs: &Equation, rhs: &Equation) -> Equation {
    let mut result = Equation::new();
    lhs.iter()
        .filter(|it| !rhs.contains(it))
        .for_each(|it| { result.insert(*it); });
    rhs.iter()
        .filter(|it| !lhs.contains(it))
        .for_each(|it| { result.insert(*it); });
    result
}