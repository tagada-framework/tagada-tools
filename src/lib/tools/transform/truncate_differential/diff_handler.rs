use std::collections::{BTreeSet, HashMap};
use tagada_structs::common::Domain;
use tagada_structs::differential::{DifferentialStateNode, DifferentialStateNodeUID};
use crate::DomainExtensions;
use crate::tools::transform::truncate_differential::diff::Diff;
use crate::tools::transform::truncate_differential::TruncatedDifferentialGraphBuilder;

pub(crate) struct DiffHandler {
    diff_vars: HashMap<Diff<DifferentialStateNodeUID>, DifferentialStateNodeUID>,
}

impl DiffHandler {
    pub(crate) fn new() -> DiffHandler {
        DiffHandler { diff_vars: HashMap::new() }
    }

    pub(crate) fn get(&mut self, builder: &mut TruncatedDifferentialGraphBuilder, states: &mut Vec<DifferentialStateNode>, lhs: DifferentialStateNodeUID, rhs: DifferentialStateNodeUID) -> DifferentialStateNodeUID {
        *self.diff_vars.entry(Diff(lhs, rhs))
            .or_insert_with_key(|&Diff(lhs, rhs)|
                DiffHandler::post_dynamic_xor(builder, states, lhs, rhs)
            )
    }

    pub(crate) fn set(&mut self, diff: Diff<DifferentialStateNodeUID>, var: DifferentialStateNodeUID) {
        self.diff_vars.insert(diff, var);
    }

    fn post_dynamic_xor(
        builder: &mut TruncatedDifferentialGraphBuilder,
        states: &mut Vec<DifferentialStateNode>,
        lhs: DifferentialStateNodeUID,
        rhs: DifferentialStateNodeUID,
    ) -> DifferentialStateNodeUID {
        let lhs_dom = domain_of(&states[lhs.uid]);
        let rhs_dom = domain_of(&states[rhs.uid]);
        let diff_var_uid = DifferentialStateNodeUID { uid: states.len() };
        let diff_var = DifferentialStateNode::Variable { domain: xor_doms(&lhs_dom, &rhs_dom) };
        states.push(diff_var);
        let truncated_function_uid = builder.truncated_xor();
        builder.create_transition(
            &states,
            &vec![lhs, rhs],
            truncated_function_uid,
            &vec![diff_var_uid],
        );
        diff_var_uid
    }

    pub(crate) fn finish(self) -> HashMap<Diff<DifferentialStateNodeUID>, DifferentialStateNodeUID> {
        self.diff_vars
    }
}

fn domain_of(state: &DifferentialStateNode) -> Domain {
    match state {
        DifferentialStateNode::Variable { domain } => domain.clone(),
        DifferentialStateNode::Constant { value } => {
            let mut values = BTreeSet::new();
            values.insert(*value);
            Domain::Sparse { values }
        }
        _ => panic!()
    }
}

fn xor_doms(lhs: &Domain, rhs: &Domain) -> Domain {
    let mut values = BTreeSet::new();
    for l_value in lhs.values() {
        for r_value in rhs.values() {
            values.insert(l_value ^ r_value);
        }
    }
    Domain::Sparse { values }
}