use std::collections::{HashSet};
use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use tagada_structs::specification::{SpecificationGraph, StateNode, StateNodeUID, Transition};
use tagada_structs::TagadaGraph;
use clap::Parser;
use crate::encrypt::topological_sort_transitions;

#[derive(Parser)]
#[command(about = "Transform a specification graph into another specification graph without the keyschedule.", long_about = None)]
pub struct SingleKeyArgs {
    pub json_path: Option<PathBuf>,
}

pub fn bin_single_key(args: SingleKeyArgs) -> io::Result<()> {
    let SingleKeyArgs {
        json_path
    } = args;

    let cipher: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    let graph = match cipher {
        TagadaGraph::Specification { graph: cipher } => {
            single_key(cipher)
        }
        _ => panic!("Single key transformation is only applicable to specification graphs"),
    };
    println!("{}", serde_json::ser::to_string(&TagadaGraph::Specification { graph })?);
    Ok(())
}

fn single_key(mut g: SpecificationGraph) -> SpecificationGraph {
    let mut keyschelule = HashSet::new();
    for key_word in g.key.iter() {
        keyschelule.insert(*key_word);
    }

    for Transition { inputs, outputs, .. } in topological_sort_transitions(&g) {
        if inputs.iter().all(|it| keyschelule.contains(it) || g.states[it.uid].is_constant()) {
            for output in outputs.iter() {
                keyschelule.insert(*output);
            }
        }
    }

    let zero_const = g.states.iter()
        .enumerate()
        .find(|(_, it)| is_zero_const(it))
        .map(|it| StateNodeUID { uid: it.0 });

    let zero_const =
        if let Some(uid) = zero_const {
            uid
        } else {
            let uid = StateNodeUID { uid: g.states.len() };
            g.states.push(StateNode::Constant { value: 0 });
            uid
        };

    // Retains only operations that are not in the keyschedule
    {
        let SpecificationGraph {
            transitions,
            states,
            ..
        } = &mut g;

        transitions
            .retain(|it| it.inputs.iter().any(|input| !keyschelule.contains(input) && !states[input.uid].is_constant()));
    }
    // Transform round key elements to zero constants
    for Transition { inputs, .. } in g.transitions.iter_mut() {
        inputs.iter_mut().for_each(|input| {
            if keyschelule.contains(input) {
                *input = zero_const;
            }
        });
    }

    g.key.iter_mut().for_each(|it| *it = zero_const);
    g.names.iter_mut().for_each(|(_, it)| if keyschelule.contains(it) { *it = zero_const });
    g.blocks.iter_mut().for_each(|block| {
        block.1.iter_mut().for_each(|it| if keyschelule.contains(it) { *it = zero_const });
    });

    g
}

fn is_zero_const(s: &StateNode) -> bool {
    match s {
        StateNode::Constant { value: 0 } => true,
        _ => false
    }
}
