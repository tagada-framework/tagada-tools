use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use itertools::Itertools;
use tagada_structs::common::{Domain, Value, Version};
use tagada_structs::differential::DifferentialFunction::{DDT, Linear};
use tagada_structs::differential::{DifferentialFunctionNode, DifferentialFunctionNodeUID, DifferentialGraph, DifferentialStateNode, DifferentialStateNodeUID, DifferentialTransition, Hundredth, MinusLog2Probability};
use tagada_structs::specification::{Function, FunctionNode, LinearFunction, SpecificationGraph, StateNode, StateNodeUID, Transition};
use tagada_structs::specification::LinearFunction::BitwiseXOR;
use tagada_structs::TagadaGraph;
use clap::Parser;
use tagada_structs::common::Domain::{Range, Sparse};
use crate::DomainExtensions;

#[derive(Parser)]
#[command(about = "Transform a specification graph into a differential graph.", long_about = None)]
pub struct DeriveArgs {
    pub json_path: Option<PathBuf>,
}

pub fn bin_derive(args: DeriveArgs) -> io::Result<()> {
    let DeriveArgs { json_path } = args;
    let graph: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    let graph = match graph {
        TagadaGraph::Specification { graph } => make_differential_graph(graph),
        _ => panic!("Differential transformation is only applicable to specification graphs")
    };

    println!("{}", serde_json::ser::to_string(&TagadaGraph::Differential { graph })?);
    Ok(())
}

fn make_differential_graph(graph: SpecificationGraph) -> DifferentialGraph {
    let mut builder = DifferentialGraphBuilder::new();

    let SpecificationGraph {
        states,
        functions,
        transitions,
        plaintext,
        key,
        ciphertext,
        mut names,
        mut blocks,
        ..
    } = graph;
    let mut sbox_inputs = Vec::new();
    let mut sbox_outputs = Vec::new();
    for Transition { inputs, function, outputs } in &transitions {
        let mut inputs = inputs.clone();
        let FunctionNode {
            domain,
            co_domain,
            function
        } = &functions[function.uid];
        let mut domain = domain.clone();
        let differential_function_uid = match function {
            Function::SBox { lookup_table } => {
                assert_eq!(domain.len(), 1);
                assert_eq!(co_domain.len(), 1);
                let sbox_idx = sbox_inputs.len();
                names.insert(format!("in_{}", sbox_idx), inputs[0]);
                sbox_inputs.push(inputs[0]);
                names.insert(format!("out_{}", sbox_idx), outputs[0]);
                sbox_outputs.push(outputs[0]);
                builder.get_or_create_ddt(&domain[0], &co_domain[0], lookup_table)
            }
            Function::Linear { linear } => {
                if linear == &BitwiseXOR {
                    let constants = inputs.iter()
                        .positions(|it| states[it.uid].is_constant())
                        .collect::<Vec<_>>();
                    constants.iter().rev().for_each(|&pos| {
                        domain.swap_remove(pos);
                        inputs.swap_remove(pos);
                    });
                }
                builder.get_or_create_linear(&domain, co_domain, linear)
            }
        };
        builder.create_transition(&states, &inputs, differential_function_uid, outputs);
    }
    blocks.insert(String::from("__SBox_Inputs__"), sbox_inputs);
    blocks.insert(String::from("__SBox_Outputs__"), sbox_outputs);
    builder.redirect_plaintext(&plaintext);
    builder.redirect_key(&key);
    builder.redirect_ciphertext(&ciphertext);
    builder.redirect_names(&names);
    builder.redirect_blocks(&blocks);
    builder.build()
}

trait Differentiable<T> {
    fn differential(&self) -> T;
}

fn is_power_of_two(n: usize) -> bool {
    n != 0 && (n & (n - 1)) == 0
}

impl Differentiable<Domain> for Domain {
    fn differential(&self) -> Domain {
        if self.len() == 0 {
            return Range { min: 0, max: 0 };
        }

        let (min, max) = match self {
            &Range { min, max } => (min, max),
            Sparse { values } => {
                let min = values.iter().min().cloned().unwrap();
                let max = values.iter().min().cloned().unwrap() + 1;
                (min, max)
            }
        };
        if min == 0 && is_power_of_two(max) {
            return Range { min, max };
        }

        let mut differential_values = BTreeSet::new();
        for x in self.values() {
            for y in self.values() {
                differential_values.insert(x ^ y);
            }
        }
        if differential_values.len() > 0 {
            let min = differential_values.iter().min().cloned().unwrap();
            let max = differential_values.iter().max().cloned().unwrap() + 1;
            if (min..max).all(|it| differential_values.contains(&it)) {
                return Range { min, max };
            } else {
                Sparse { values: differential_values }
            }
        } else {
            Range { min: 0, max: 0 }
        }
    }
}

struct DifferentialGraphBuilder {
    graph: DifferentialGraph,
    ddts: HashMap<(Domain, Domain, Vec<Value>), DifferentialFunctionNodeUID>,
    linear_functions: HashMap<(Vec<Domain>, Vec<Domain>, LinearFunction), DifferentialFunctionNodeUID>,
    states: HashMap<StateNodeUID, DifferentialStateNodeUID>,
}

impl DifferentialGraphBuilder {
    fn new() -> DifferentialGraphBuilder {
        DifferentialGraphBuilder {
            graph: DifferentialGraph {
                version: Version(0, 10, 0),
                states: Vec::new(),
                functions: Vec::new(),
                transitions: Vec::new(),
                plaintext: Vec::new(),
                key: Vec::new(),
                ciphertext: Vec::new(),
                names: HashMap::new(),
                blocks: HashMap::new(),
            },
            ddts: HashMap::new(),
            linear_functions: HashMap::new(),
            states: HashMap::new(),
        }
    }

    pub fn create_transition(&mut self, states: &Vec<StateNode>, inputs: &Vec<StateNodeUID>, function: DifferentialFunctionNodeUID, outputs: &Vec<StateNodeUID>) {
        let differential_inputs = inputs.iter()
            .map(|it| self.get_or_create_state(it, &states[it.uid]))
            .collect();
        let differential_outputs = outputs.iter()
            .map(|it| self.get_or_create_state(it, &states[it.uid]))
            .collect();

        self.graph.transitions.push(
            DifferentialTransition {
                inputs: differential_inputs,
                function,
                outputs: differential_outputs,
            });
    }

    pub fn redirect_plaintext(&mut self, plaintext: &Vec<StateNodeUID>) {
        self.graph.plaintext = plaintext.iter()
            .map(|it| self.states[it])
            .collect();
    }

    pub fn redirect_key(&mut self, key: &Vec<StateNodeUID>) {
        self.graph.key = key.iter()
            .filter_map(|it| self.states.get(it).cloned())
            .collect();
    }

    pub fn redirect_ciphertext(&mut self, ciphertext: &Vec<StateNodeUID>) {
        self.graph.ciphertext = ciphertext.iter()
            .map(|it| self.states[it])
            .collect();
    }

    pub fn redirect_names(&mut self, names: &HashMap<String, StateNodeUID>) {
        for (name, state_node_uid) in names.iter() {
            self.redirect_name(name, state_node_uid);
        }
    }

    fn redirect_name(&mut self, name: &String, uid: &StateNodeUID) {
        if let Some(differential_state_node) = self.states.get(uid) {
            self.graph.names.insert(name.clone(), *differential_state_node);
        }
    }

    pub fn redirect_blocks(&mut self, blocks: &HashMap<String, Vec<StateNodeUID>>) {
        for (name, block) in blocks.iter() {
            self.redirect_block(name, block);
        }
    }

    fn redirect_block(&mut self, name: &String, block: &Vec<StateNodeUID>) {
        let block = block.iter()
            .filter_map(|it| self.states.get(it))
            .cloned()
            .collect::<Vec<_>>();
        if !block.is_empty() {
            self.graph.blocks.insert(name.clone(), block);
        }
    }

    pub fn get_or_create_ddt(&mut self, domain: &Domain, co_domain: &Domain, lookup_table: &Vec<Value>) -> DifferentialFunctionNodeUID {
        let DifferentialGraphBuilder {
            graph,
            ddts,
            ..
        } = self;

        *ddts.entry((domain.clone(), co_domain.clone(), lookup_table.clone()))
            .or_insert_with_key(|it| {
                let ddt = DifferentialGraphBuilder::compute_ddt(&it.0, &it.1, &it.2);
                let differential_node_uid = DifferentialFunctionNodeUID { uid: graph.functions.len() };
                graph.functions.push(ddt);
                differential_node_uid
            })
    }

    fn compute_ddt(domain: &Domain, co_domain: &Domain, s: &Vec<Value>) -> DifferentialFunctionNode {
        let diff_domain = domain.differential();
        let diff_co_domain = co_domain.differential();
        let mut nb_transitions = vec![vec![0; diff_co_domain.len()]; diff_domain.len()];
        for x in domain.values() {
            for delta_in in diff_domain.values() {
                let delta_out = s[x] ^ s[x ^ delta_in];
                assert!(diff_co_domain.contains(&delta_out));
                nb_transitions[delta_in][delta_out] += 1;
            }
        }

        let mut probabilities = BTreeMap::new();
        for delta_in in diff_domain.values() {
            for delta_out in diff_co_domain.values() {
                if nb_transitions[delta_in][delta_out] > 0 {
                    let probability = nb_transitions[delta_in][delta_out] as f64 / domain.len() as f64;
                    probabilities.insert((delta_in, delta_out), MinusLog2Probability(Hundredth((-probability.log2() * 100.0) as usize)));
                }
            }
        }
        DifferentialFunctionNode {
            domain: vec![diff_domain],
            co_domain: vec![diff_co_domain],
            function: DDT {
                probabilities
            },
        }
    }

    pub fn get_or_create_linear(&mut self, domain: &Vec<Domain>, co_domain: &Vec<Domain>, linear: &LinearFunction) -> DifferentialFunctionNodeUID {
        let DifferentialGraphBuilder {
            graph,
            linear_functions,
            ..
        } = self;

        *linear_functions.entry((domain.clone(), co_domain.clone(), linear.clone()))
            .or_insert_with_key(|it| {
                let diff_domain = it.0.iter().map(|it| it.differential()).collect();
                let diff_co_domain = it.1.iter().map(|it| it.differential()).collect();

                let linear_diff_function = DifferentialFunctionNode {
                    domain: diff_domain,
                    co_domain: diff_co_domain,
                    function: Linear { linear: it.2.clone() },
                };
                let differential_node_uid = DifferentialFunctionNodeUID { uid: graph.functions.len() };
                graph.functions.push(linear_diff_function);
                differential_node_uid
            })
    }

    pub fn get_or_create_state(&mut self, uid: &StateNodeUID, state_node: &StateNode) -> DifferentialStateNodeUID {
        let DifferentialGraphBuilder {
            graph,
            states,
            ..
        } = self;

        *states.entry(*uid).or_insert_with(|| {
            let differential_state_node_uid = DifferentialStateNodeUID { uid: graph.states.len() };
            let differential_state_node = match state_node {
                StateNode::Variable { domain } => DifferentialStateNode::Variable { domain: domain.differential() },
                &StateNode::Constant { value } => DifferentialStateNode::Constant { value }
            };
            graph.states.push(differential_state_node);
            differential_state_node_uid
        })
    }

    fn build(self) -> DifferentialGraph {
        self.graph
    }
}

