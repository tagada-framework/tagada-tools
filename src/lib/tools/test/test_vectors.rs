extern crate core;

use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use alphanumeric_sort::compare_str;
use clap::error::ErrorKind;
use clap::{CommandFactory};
use colored::Colorize;
use itertools::Itertools;
use clap::Parser;
use tagada_structs::TagadaGraph;
use crate::cli::Base;
use crate::encrypt::{encrypt, Values};

#[derive(Parser)]
pub struct TestVectorArgs {
    pub json_path: Option<PathBuf>,
    #[arg(short, long, required = true, use_value_delimiter = true)]
    pub key_values: Vec<String>,
    #[arg(short, long, required = true, use_value_delimiter = true)]
    pub plaintext_values: Vec<String>,
    #[arg(short, long, required = true)]
    pub base: Base,
    #[arg(short, long)]
    pub trace: bool,
    #[arg(short, long, default_value_t = 1)]
    pub nb_encryption: usize,
    #[arg(short, long, use_value_delimiter = true)]
    pub expected_ciphertext: Option<Vec<String>>,
}

pub fn bin_test_vectors(args: TestVectorArgs) -> io::Result<()> {
    let TestVectorArgs {
        json_path,
        key_values,
        plaintext_values,
        base,
        trace,
        nb_encryption,
        expected_ciphertext,
    } = args;

    let cipher: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    if nb_encryption == 0 {
        let mut cmd = TestVectorArgs::command();
        cmd.error(ErrorKind::InvalidValue, "The number of encryption must be strictly positive").exit();
    }

    match cipher {
        TagadaGraph::Specification { graph: cipher} => {
            let key_values = base.parse_vec(&key_values);
            let mut plaintext_values = base.parse_vec(&plaintext_values);
            let mut values = Values::new(cipher.states.len());
            for encryption in 0..nb_encryption {
                values.clear();
                encrypt(&cipher, &key_values, &plaintext_values, &mut values);
                println!("Encryption {} ================================", encryption + 1);
                if trace {
                    let sorted_blocks = cipher.blocks.iter()
                        .sorted_by(|lhs, rhs| compare_str(&lhs.0, &rhs.0));
                    for (block_name, block_vars) in sorted_blocks {
                        println!("{} {}", block_name, values.fmt_of(&base, &block_vars));
                    }
                } else {
                    println!("__CIPHERTEXT__ {}", values.fmt_of(&base, &cipher.ciphertext));
                    println!("__KEY__ {}", values.fmt_of(&base, &cipher.key));
                    println!("__PLAINTEXT__ {}", values.fmt_of(&base, &cipher.plaintext));
                }
                plaintext_values = values.of(&cipher.ciphertext);
            }

            if let Some(expected_ciphertext) = expected_ciphertext {
                let expected_ciphertext_values = base.parse_vec(&expected_ciphertext);
                let computed_ciphertext_values = values.of(&cipher.ciphertext);
                if expected_ciphertext_values == computed_ciphertext_values {
                    println!("{}", "OK".green());
                } else {
                    println!("{}", "Failure".red());
                    let comparison_text = format!(
                        "Expected: [{}]\nGiven:    [{}]",
                        expected_ciphertext_values.iter().map(|it| format!("{:4}", it)).join(", "),
                        computed_ciphertext_values.iter().map(|it| format!("{:4}", it)).join(", ")
                    );
                    println!("{}", comparison_text.red());
                }
            }
        },
        _ => panic!("Vector testing is only applicable to specification graphs"),
    }
    Ok(())
}

