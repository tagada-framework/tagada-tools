use std::collections::HashMap;
use std::env::temp_dir;
use std::ffi::OsStr;
use std::fs::File;
use std::io;
use std::path::{Path, PathBuf};
use tagada_structs::common::Value;
use uuid::Uuid;

pub mod gen_graphviz;
pub mod backends;
pub mod mzn_builder;
pub mod search;
pub mod transform;
pub mod test;
pub mod evaluate;

pub type Assignment = HashMap<String, Value>;

pub fn tempfile() -> io::Result<(PathBuf, File)> {
    let path_buf = temp_dir().join(Path::new(&Uuid::new_v4().to_string()));
    File::create(path_buf.clone()).map(|it| (path_buf, it))
}

pub fn tempfile_with_extension<S: AsRef<OsStr> + ?Sized>(extension: &S) -> io::Result<(PathBuf, File)> {
    let path_buf = temp_dir().join(Path::new(&Uuid::new_v4().to_string())).with_extension(extension);
    File::create(path_buf.clone()).map(|it| (path_buf, it))
}