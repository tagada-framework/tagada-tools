use std::io::{BufWriter, Write};
use std::process::{Command, Stdio};
use itertools::Itertools;
use quine_mc_cluskey::Bool;
use quine_mc_cluskey::Bool::{And, Not, Or};

pub fn simplify(truth_table: &Vec<Vec<bool>>) -> Bool {
    let mut content = String::new();
    content += format!(".i {}\n", truth_table[0].len()).as_str();
    content += ".o 1\n";
    cartesian_product(&vec![vec![false, true]; truth_table[0].len()], &mut |tuple| {
        if !truth_table.contains(tuple) {
            content += tuple.iter().map(|it| if *it { "1" } else { "0" }).join("").as_str();
            content += " 1\n";
        }
    });
    content += ".e";
    let espresso = std::env::var("ESPRESSO_AB")
        .unwrap_or(String::from("espresso"));
    let mut espresso = Command::new(&espresso)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();
    {
        let stdin = espresso.stdin.as_mut().unwrap();
        let mut buf_writer = BufWriter::new(stdin);
        buf_writer.write_all(content.as_bytes()).unwrap();
    }

    let output = espresso.wait_with_output().unwrap();
    let result = String::from_utf8(output.stdout).unwrap();

    let mut cnf = Vec::new();
    for line in result.split('\n') {
        let line = line.trim();
        if !line.is_empty() && !line.starts_with('.') {
            let mut disjunction = Vec::new();
            let (terms, _) = line.split_at(line.chars().position(|c| c == ' ').unwrap());
            for (v, term) in terms.chars().enumerate() {
                let term = match term {
                    '0' => { Some(Bool::Term(v as u8)) }
                    '1' => { Some(Not(Box::new(Bool::Term(v as u8)))) }
                    '-' => { None }
                    _ => panic!("unsupported symbol {} in espresso result parsing", term)
                };
                if let Some(t) = term {
                    disjunction.push(t);
                }
            }
            cnf.push(Or(disjunction));
        }
    }
    And(cnf)
}

fn cartesian_product<F>(domain: &Vec<Vec<bool>>, on_each: &mut F) where F: FnMut(&Vec<bool>) {
    cartesian_product_recursive(&domain, 0, &mut vec![false; domain.len()], on_each);
}

fn cartesian_product_recursive<F>(domain: &Vec<Vec<bool>>, depth: usize, arr: &mut Vec<bool>, on_each: &mut F) where F: FnMut(&Vec<bool>) {
    if depth == domain.len() {
        on_each(arr)
    } else {
        for &value in &domain[depth] {
            arr[depth] = value;
            cartesian_product_recursive(domain, depth + 1, arr, on_each);
        }
    }
}