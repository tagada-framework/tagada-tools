use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use measure_time::info_time;
use crate::tools::backends::minizinc::MiniZinc;
use crate::tools::backends::{FznSolver, path_or_default};

const ERR_MSG: &'static str = "failed to execute gurobi";

pub struct Gurobi {
    minizinc_path: String,
    dll_path: String,
    nb_threads: usize
}

impl Gurobi {
    pub fn new(mzn: &MiniZinc, dll_path: Option<PathBuf>, nb_threads: Option<usize>) -> Gurobi {
        let gurobi = std::env::var("GUROBI_DLL")
            .unwrap_or(String::from("libgurobi.so.10.0.1"));
        Gurobi {
            minizinc_path: mzn.path.clone(),
            dll_path: path_or_default(&dll_path, &gurobi),
            nb_threads: nb_threads.unwrap_or(1).max(1)
        }
    }
}

impl FznSolver for Gurobi {



    fn solve(&self, model: &Path) -> (PathBuf, File) {
        info_time!("gurobi solving");

        let path = model.with_extension("sol");
        let file_out = File::create(&path)
            .unwrap();
        let status = Command::new(&self.minizinc_path)
            .args(&[
                "--solver", "gurobi",
                "--gurobi-dll", &self.dll_path,
                "-p", &self.nb_threads.to_string(),
                model.to_str().unwrap(),
            ])
            .stdout(Stdio::from(file_out))
            .status()
            .expect(ERR_MSG);
        if !status.success() {
            panic!("{}", ERR_MSG);
        }
        let file_read = File::open(&path).unwrap();
        let gurobi_output = fs::read_to_string(&path).expect("cannot open picat stdout file");
        if !gurobi_output.contains(MiniZinc::SOLUTION_SEPARATOR) && !gurobi_output.contains(MiniZinc::UNSAT_MSG) {
            panic!("{}\n{}", ERR_MSG, gurobi_output);
        }
        (path, file_read)
    }

    fn tag(&self) -> &'static str {
        "gurobi"
    }
}
