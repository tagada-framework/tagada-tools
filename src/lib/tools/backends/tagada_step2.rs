use std::io::BufWriter;
use std::path::PathBuf;
use std::process::{Command};
use measure_time::info_time;
use crate::tools::backends::path_or_default;
use crate::tools::{Assignment, tempfile};
use crate::tools::search::best_differential_characteristic::NullableProbability;

const RUNTIME_ERROR: &'static str = "failed to execute tagada-step2";
const PARSING_ERROR: &'static str = "failed to parse tagada-step2";

pub struct TagadaStep2 {
    jdk: String,
    jar_path: String,
}

impl TagadaStep2 {
    pub fn new(jdk: &Option<PathBuf>, jar_path: &Option<PathBuf>) -> TagadaStep2 {
        let step2 = std::env::var("STEP2")
            .unwrap_or(String::from("tagada-step2-1.0.0-all.jar"));
        TagadaStep2 {
            jdk: path_or_default(jdk, "java"),
            jar_path: path_or_default(jar_path, &step2),
        }
    }

    pub fn bounded_optimal_differential_characteristic(
        &self,
        differential_graph: &PathBuf,
        s1: &Assignment,
        lb: NullableProbability,
    ) -> Option<Assignment> {
        let (assignment_path, assignment_file) = tempfile().unwrap();
        {
            let writer = BufWriter::new(&assignment_file);
            serde_json::to_writer(writer, s1)
                .expect("cannot serialize step-1 solution");
        }

        info_time!("tagada step-2");
        let result = Command::new(&self.jdk)
            .args(&[
                "-jar", &self.jar_path,
                "-c", differential_graph.to_str().unwrap(),
                "-s1", assignment_path.to_str().unwrap(),
                "-lb", &lb.as_step2_cli_arg()]
            )
            .output()
            .expect(RUNTIME_ERROR);
        let output = String::from_utf8(result.stdout).unwrap();
        serde_json::de::from_str(&output)
            .expect(PARSING_ERROR)
    }
}