pub mod tagada_step2;
pub mod minizinc;
pub mod picat;
pub mod or_tools;
pub mod espresso_ab;
pub mod gurobi;

use std::fs::File;
use std::path::{Path, PathBuf};

fn path_or_default(path: &Option<PathBuf>, default: &str) -> String {
    String::from(if let Some(path) = path {
        path.to_str().unwrap()
    } else {
        default
    })
}

pub trait FznSolver {
    fn solve(&self, model: &Path) -> (PathBuf, File);
    fn tag(&self) -> &'static str;
}
