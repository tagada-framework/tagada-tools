use std::fs::File;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use log::trace  ;
use measure_time::info_time;
use crate::tools::Assignment;
use crate::tools::backends::{FznSolver, path_or_default};

const COMPILATION_ERROR: &'static str = "failed to execute minizinc compilation";
const FORMATTING_ERROR: &'static str = "failed to execute minizinc output formatting";
const PARSING_ERROR: &'static str = "failed to parse minizinc output";

pub struct MiniZinc {
    pub path: String,
}

impl MiniZinc {
    pub const END_SEARCH: &'static str = "==========";
    pub const UNSAT_MSG: &'static str = "=====UNSATISFIABLE=====";
    pub const SOLUTION_SEPARATOR: &'static str = "----------";

    pub fn new(path: &Option<PathBuf>) -> MiniZinc {
        let minizinc = std::env::var("MINIZINC")
            .unwrap_or(String::from("minizinc"));
        MiniZinc { path: path_or_default(path, &minizinc) }
    }

    pub fn compile_for(&self, model: &Path, solver: &Box<dyn FznSolver>) -> (PathBuf, PathBuf) {
        info_time!("minizinc compilation");
        let status = Command::new(&self.path)
            .args(&["--compile", model.to_str().unwrap(), "--solver", solver.tag(), "-Werror"])
            .status()
            .expect(COMPILATION_ERROR);
        if !status.success() {
            panic!("{}", COMPILATION_ERROR);
        }
        trace!("minizinc compilation output: {:?}", (model.with_extension("fzn"), model.with_extension("ozn")));
        (model.with_extension("fzn"), model.with_extension("ozn"))
    }

    pub fn format(&self, output: File, ozn_file: &Path) -> Option<Assignment> {
        let output = Command::new(&self.path)
            .args(&[
                "--ozn-file", ozn_file.to_str().unwrap(), "--no-output-comments",
            ])
            .stdin(Stdio::from(output))
            .output()
            .expect(FORMATTING_ERROR);

        let solutions = String::from_utf8(output.stdout).unwrap();
        let mut solutions = solutions
            .replace("\n", "")
            .replace(&(String::from(MiniZinc::SOLUTION_SEPARATOR) + MiniZinc::END_SEARCH), "")
            .replace(MiniZinc::SOLUTION_SEPARATOR, ",")
            .replace(MiniZinc::UNSAT_MSG, "")
            .replace("true", "1")
            .replace("false", "0");
        solutions = format!("[{}]", solutions.strip_suffix(",").unwrap_or(&solutions));
        let mut solutions = serde_json::de::from_str::<Vec<Assignment>>(&solutions)
            .expect(PARSING_ERROR);

        solutions.pop()
    }
}