use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use measure_time::info_time;
use crate::tools::backends::minizinc::MiniZinc;
use crate::tools::backends::{FznSolver, path_or_default};

const ERR_MSG: &'static str = "failed to execute or_tools";

pub struct OrTools {
    path: String,
}

impl OrTools {
    pub fn new(
        path: &Option<PathBuf>
    ) -> OrTools {
        let or_tools = std::env::var("FZN_ORTOOLS")
            .unwrap_or(String::from("fzn-ortools"));
        OrTools {
            path: path_or_default(path, &or_tools),
        }
    }
}

impl FznSolver for OrTools {
    fn solve(&self, model: &Path) -> (PathBuf, File) {
        info_time!("or_tools solving");
        let path = model.with_extension("sol");
        let file_out = File::create(&path)
            .unwrap();
        let status = Command::new(&self.path)
            .args(&[
                model.to_str().unwrap()
            ])
            .stdout(Stdio::from(file_out))
            .status()
            .expect(ERR_MSG);
        if !status.success() {
            panic!("{}", ERR_MSG);
        }
        let file_read = File::open(&path).unwrap();
        let or_tools_output = fs::read_to_string(&path).expect("cannot open picat stdout file");
        if !or_tools_output.contains(MiniZinc::END_SEARCH) && !or_tools_output.contains(MiniZinc::UNSAT_MSG) {
            panic!("{}\n{}", ERR_MSG, or_tools_output);
        }
        (path, file_read)
    }

    fn tag(&self) -> &'static str {
        "mip"
    }
}