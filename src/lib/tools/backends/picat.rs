use std::fs;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use measure_time::info_time;
use crate::tools::backends::minizinc::MiniZinc;
use crate::tools::backends::{FznSolver, path_or_default};

const ERR_MSG: &'static str = "failed to execute picat with fzn_picat_sat";

pub struct Picat {
    path: String,
    fzn_picat_sat: String,
}

impl Picat {
    pub fn new(
        path: &Option<PathBuf>,
        fzn_picat_sat: &Option<PathBuf>) -> Picat {
        let picat_default = std::env::var("PICAT")
            .unwrap_or(String::from("picat"));
        let fzn_picat_sat_default = std::env::var("FZN_PICAT_SAT")
            .unwrap_or(String::from("fzn_picat_sat.pi"));
        Picat {
            path: path_or_default(path, &picat_default),
            fzn_picat_sat: path_or_default(fzn_picat_sat, &fzn_picat_sat_default),
        }
    }
}

impl FznSolver for Picat {
    fn solve(&self, model: &Path) -> (PathBuf, File) {
        info_time!("picat solving");

        let path = model.with_extension("sol");
        let file_out = File::create(&path)
            .unwrap();
        let status = Command::new(&self.path)
            .args(&[
                &self.fzn_picat_sat,
                model.to_str().unwrap(),
            ])
            .stdout(Stdio::from(file_out))
            .status()
            .expect(ERR_MSG);
        if !status.success() {
            panic!("{}", ERR_MSG);
        }
        let file_read = File::open(&path).unwrap();
        let picat_output = fs::read_to_string(&path).expect("cannot open picat stdout file");
        if !picat_output.contains(MiniZinc::SOLUTION_SEPARATOR) && !picat_output.contains(MiniZinc::UNSAT_MSG) {
            panic!("{}\n{}", ERR_MSG, picat_output);
        }
        (path, file_read)
    }

    fn tag(&self) -> &'static str {
        "cp"
    }
}