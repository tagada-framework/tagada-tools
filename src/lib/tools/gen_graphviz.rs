use std::fs::File;
use std::io;
use std::io::{BufReader, Read, stdin};
use std::path::PathBuf;
use tagada_structs::TagadaGraph;
use clap::Parser;
use crate::dag_graphviz::Graphviz;


#[derive(Parser)]
pub struct GenerateGraphvizArgs {
    pub json_path: Option<PathBuf>
}

pub fn bin_generate_graphviz(args: GenerateGraphvizArgs) -> io::Result<()>{
    let GenerateGraphvizArgs { json_path } = args;
    let graph: TagadaGraph = if let Some(json_path) = json_path {
        let file = File::open(json_path)?;
        let reader = BufReader::new(file);
        serde_json::from_reader(reader)?
    } else {
        let mut json = String::new();
        stdin().read_to_string(&mut json)?;
        serde_json::from_str(&json)?
    };

    match graph {
        TagadaGraph::Specification { graph } => println!("{}", Graphviz::from(&graph).0),
        TagadaGraph::Differential { graph } => println!("{}", Graphviz::from(&graph).0),
        TagadaGraph::TruncatedDifferential { graph } => println!("{}", Graphviz::from(&graph).0),
    };
    Ok(())
}