use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fmt::Display;
use clap::ValueEnum;
use itertools::Itertools;
use quine_mc_cluskey::Bool;
use quine_mc_cluskey::Bool::{And, Not, Or};
use tagada_structs::common::{Domain, Value};
use tagada_structs::differential::MinusLog2Probability;
use tagada_structs::truncated_differential::TruncatedDifferentialStateNodeUID;
use Encoding::TABLE;
use crate::tools::Assignment;
use crate::tools::backends::espresso_ab;
use crate::tools::mzn_builder::Encoding::{DNF, CNF, SUM};

pub struct MiniZincBuilder<'t > {
    model: String,
    sum_repr: BTreeMap<Vec<Vec<bool>>, (bool, String)>,
    dnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
    cnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
    table: &'t mut BTreeMap<Vec<Vec<bool>>, String>,
}

pub fn var_name(uid: &TruncatedDifferentialStateNodeUID) -> String {
    format!("var_{}", uid.uid)
}

pub fn eq_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} == {}", lhs, rhs)
}

pub fn ne_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} != {}", lhs, rhs)
}

pub fn not<I: Display>(inner: I) -> String {
    format!("not({})", inner)
}

pub fn lt_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} < {}", lhs, rhs)
}

pub fn le_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} <= {}", lhs, rhs)
}

pub fn gt_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} > {}", lhs, rhs)
}

pub fn ge_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} >= {}", lhs, rhs)
}

pub fn mul_expr<L: Display, R: Display>(lhs: L, rhs: R) -> String {
    format!("{} * {}", lhs, rhs)
}

pub fn paren<E: Display>(expr: E) -> String {
    format!("({})", expr)
}

pub fn sum_expr<'a, I: Iterator<Item=&'a TruncatedDifferentialStateNodeUID>>(iter: I) -> String {
    format!("sum([{}])", iter.map(var_name).join(", "))
}

pub fn conjunction_expr<'a, 'b, I: Iterator<Item=(&'a TruncatedDifferentialStateNodeUID, &'b bool)>>(iter: I) -> String {
    iter.map(|(uid, &value)| {
        if value {
            var_name(uid)
        } else {
            not(var_name(uid))
        }
    }).join(" /\\ ")
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, ValueEnum)]
pub enum Encoding {
    SUM,
    DNF,
    CNF,
    TABLE,
}

impl <'t> MiniZincBuilder<'t> {
    pub fn new(
        dnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
        cnf: &'t mut BTreeMap<Vec<Vec<bool>>, Bool>,
        table: &'t mut BTreeMap<Vec<Vec<bool>>, String>,
    ) -> MiniZincBuilder<'t> {
        MiniZincBuilder {
            model: String::from("include \"table.mzn\";\n"),
            sum_repr: BTreeMap::new(),
            dnf,
            cnf,
            table,
        }
    }

    pub fn build(self) -> String {
        self.model
    }

    pub fn constraint<E: Display>(&mut self, expr: E) {
        self.model += format!("constraint {};\n", expr).as_str();
    }

    fn create_obj_var(&mut self, max: Value, expr: String) -> String {
        self.model += format!("var 0..{}: obj = {};\n", max, expr).as_str();
        String::from("obj")
    }

    fn create_obj_var_from_weighted_sum(&mut self, weighted_sum: &BTreeMap<MinusLog2Probability, Vec<TruncatedDifferentialStateNodeUID>>) -> (String, String) {
        let ub = weighted_sum.iter()
            .map(|(k, v)| k.0.0 * v.len())
            .sum::<usize>();
        let mut sum_parts = Vec::new();
        for (coefficient, sum) in weighted_sum.iter() {
            if coefficient.0.0 > 0 {
                let coefficient_times_sum = format!("obj_{}", coefficient.0.0);
                self.create_named_var_from_range(coefficient_times_sum.clone(), 0, sum.len() * coefficient.0.0);
                self.constraint(eq_expr(coefficient_times_sum.clone(), sum_expr(sum.iter())));
                sum_parts.push((coefficient.0.0, coefficient_times_sum));
            }
        }

        let obj = self.create_obj_var(ub, sum_parts.iter().map(|(c, cs)| mul_expr(c, cs)).join(" + "));
        if sum_parts.len() == 1 {
            (obj, sum_parts[0].1.clone())
        } else {
            (obj.clone(), obj)
        }
    }

    pub fn minimize_weighted_sum(&mut self, weighted_sum: &BTreeMap<MinusLog2Probability, Vec<TruncatedDifferentialStateNodeUID>>, sboxes: &Vec<String>) -> String {
        let (obj, inner_obj) = self.create_obj_var_from_weighted_sum(weighted_sum);
        self.model.push_str(&format!("solve :: int_search([{}], smallest, indomain_min, complete) minimize {};\n", sboxes.join(", "), inner_obj));
        obj
    }

    pub fn satisfy_weighted_sum(&mut self, weighted_sum: &BTreeMap<MinusLog2Probability, Vec<TruncatedDifferentialStateNodeUID>>, sboxes: &Vec<String>) -> String {
        let (obj, _) = self.create_obj_var_from_weighted_sum(weighted_sum);
        self.model.push_str(&format!("solve :: int_search([{}], smallest, indomain_min, complete) satisfy;\n", sboxes.join(", ")));
        obj
    }

    pub fn equality(&mut self, lhs: &TruncatedDifferentialStateNodeUID, rhs: &TruncatedDifferentialStateNodeUID) {
        self.constraint(eq_expr(var_name(lhs), var_name(rhs)))
    }

    pub fn greater_than<L: Display, R: Display>(&mut self, lhs: L, rhs: R) {
        self.constraint(gt_expr(lhs, rhs));
    }

    pub fn equal_to<L: Display, R: Display>(&mut self, lhs: L, rhs: R) {
        self.constraint(eq_expr(lhs, rhs));
    }

    pub fn not_equal_to<L: Display, R: Display>(&mut self, lhs: L, rhs: R) {
        self.constraint(ne_expr(lhs, rhs));
    }

    pub fn less_than<L: Display, R: Display>(&mut self, lhs: L, rhs: R) {
        self.constraint(lt_expr(lhs, rhs));
    }

    pub fn create_variable(&mut self, uid: &TruncatedDifferentialStateNodeUID, domain: &Domain) {
        match domain {
            &Domain::Range { min, max } => self.create_var_from_range(&uid, min, max),
            Domain::Sparse { values } => self.create_var_from_sparse(&uid, values)
        }
    }

    pub fn comment<S: AsRef<str>>(&mut self, comment: S) {
        self.model += format!("% {}\n", comment.as_ref()).as_str();
    }

    pub fn create_constant(&mut self, uid: &TruncatedDifferentialStateNodeUID, value: &Value) {
        self.model += format!("int {} = {};\n", var_name(uid), value).as_str();
    }

    fn create_named_var_from_range(&mut self, name: String, min: Value, max: Value) {
        if min == 0 && max == 1 {
            self.model += format!("var bool: {};\n", name).as_str();
        } else {
            self.model += format!("var {}..{}: {};\n", min, max, name).as_str();
        }
    }

    fn create_var_from_range(&mut self, uid: &TruncatedDifferentialStateNodeUID, min: Value, max: Value) {
        self.create_named_var_from_range(var_name(uid), min, max)
    }

    fn create_var_from_sparse(&mut self, uid: &TruncatedDifferentialStateNodeUID, values: &BTreeSet<Value>) {
        let min = *values.iter()
            .min()
            .expect("a least one value should be valid for each variable");
        let max = *values.iter()
            .max()
            .expect("a least one value should be valid for each variable");
        if min == 0 && max == 1 {
            self.model += format!("var bool: {};\n", var_name(uid)).as_str();
        } else {
            self.model += format!("var {{{:?}}}: {}", values.iter().map(|it| it.to_string()).join(","), var_name(uid)).as_str();
        }
    }

    pub fn forbid_sbox_assignment(&mut self, assignment: &Assignment, names: &HashMap<String, TruncatedDifferentialStateNodeUID>) {
        let active_sbox_inputs = assignment.iter()
            .filter_map(|(it, &value)| {
                assert!(value == 0 || value == 1);
                if value == 1 {
                    Some(var_name(&names[it]))
                } else { // value = 0
                    None
                }
            })
            .join(" /\\ ");
        if active_sbox_inputs.len() > 0 {
            // At least one active S-Box
            // we forbid the exact same assignment
            self.constraint(not(active_sbox_inputs));
        } else {
            // No active S-Boxes
            // we force to have a solution with at least one active S-Box
            self.constraint(assignment.iter().map(|(it, _)| var_name(&names[it])).join(" \\/ "))
        }
    }

    pub fn table(&mut self, scope: &Vec<TruncatedDifferentialStateNodeUID>, truth_table: &Vec<Vec<bool>>, encoding: Encoding) {
        match encoding {
            SUM => {
                let (is_truth, sum_constraint) = self.sum_repr.entry(truth_table.clone()).or_insert_with(|| {
                    let possible_values: Vec<usize> = truth_table.iter()
                        .map(|it| it.iter().filter(|it| **it).count())
                        .unique()
                        .collect();
                    let mut impossible_values: Vec<usize> = (0..scope.len() + 1).collect();
                    impossible_values.retain(|it| !possible_values.contains(it));

                    if possible_values.len() < impossible_values.len() {
                        if possible_values.len() == 1 {
                            (true, format!("== {}", possible_values[0]))
                        } else {
                            (true, format!("in {{{}}}", possible_values.iter().map(|it| it.to_string()).join(",")))
                        }
                    } else {
                        if impossible_values.len() == 1 {
                            (true, format!("!= {}", impossible_values[0]))
                        } else {
                            (false, format!("in {{{}}}", impossible_values.iter().map(|it| it.to_string()).join(",")))
                        }
                    }
                }).clone();
                let mut constraint = format!("{} {}", sum_expr(scope.iter()), sum_constraint);
                if !is_truth {
                    constraint = not(constraint);
                }
                self.constraint(constraint)
            }
            DNF => {
                let dnf = self.dnf.entry(truth_table.clone()).or_insert_with(|| {
                    let conjunctions = truth_table.iter().map(|terms| {
                        let terms = terms.iter().enumerate().map(|(v, b)| {
                            let term = Bool::Term(v as u8);
                            if *b { term } else { Not(Box::new(term)) }
                        }).collect();
                        And(terms)
                    }).collect();

                    let dnf = Or(conjunctions);
                    Or(dnf.simplify())
                }).clone();
                self.constraint(sat_formula(&dnf, scope));
            }
            CNF => {
                let cnf = self.cnf.entry(truth_table.clone()).or_insert_with(|| {
                    espresso_ab::simplify(truth_table)
                }).clone();
                self.constraint(sat_formula(&cnf, scope));
            }
            TABLE => {
                let table_uid = self.table.len();
                let table_name = {
                    let MiniZincBuilder { table, model, .. } = self;
                    table.entry(truth_table.clone()).or_insert_with(|| {
                        let name = format!("table_{}", table_uid);
                        model.push_str(&format!("array[int, int] of bool: {} = [|{}|];\n", name,
                                                truth_table.iter()
                                                    .map(|it| it.iter().map(|b| b.to_string()).join(","))
                                                    .join("|")).as_str()
                        );
                        name
                    }).clone()
                };
                self.constraint(
                    format!("table([{}], {})",
                            scope.iter().map(var_name).join(","),
                            table_name
                    )
                );
            }
        }
    }

    pub fn configure_output(&mut self, names: &HashMap<String, TruncatedDifferentialStateNodeUID>) {
        let mut output = String::new();
        for (name, uid) in names {
            output += format!("\\\"{}\\\":\\(var_{}),", name, uid.uid).as_str();
        }
        output += "\\\"obj\\\":\\(obj)";
        self.model += format!("output [\"{{{}}}\"];\n", output).as_str();
    }
}

fn sat_formula(bool: &Bool, scope: &Vec<TruncatedDifferentialStateNodeUID>) -> String {
    match bool {
        &Bool::False => String::from("false"),
        &Bool::True => String::from("true"),
        Or(bools) => paren(bools.iter().map(|it| sat_formula(it, scope)).join(" \\/ ")),
        And(bools) => paren(bools.iter().map(|it| sat_formula(it, scope)).join(" /\\ ")),
        Not(b) => not(sat_formula(b.as_ref(), scope)),
        &Bool::Term(index) => var_name(&scope[index as usize])
    }
}