use std::collections::VecDeque;
use std::ops::Index;
use itertools::Itertools;
use tagada_structs::common::Value;
use tagada_structs::specification::{Computable, FunctionNode, SpecificationGraph, StateNode, StateNodeUID, Transition};
use crate::cli::Base;
use crate::DomainExtensions;

pub fn encrypt(cipher: &SpecificationGraph, key_values: &Vec<Value>, plaintext_values: &Vec<Value>, values: &mut Values) {
    cipher.key.iter().zip_eq(key_values)
        .for_each(|(&k, &v)| {
            values.insert(k, v);
        });

    cipher.plaintext.iter().zip_eq(plaintext_values)
        .for_each(|(&k, &v)| {
            values.insert(k, v);
        });

    for (uid, state) in cipher.states.iter().enumerate() {
        if let &StateNode::Constant { value } = state {
            values.insert(StateNodeUID { uid }, value);
        }
    }
    propagate_encryption(&cipher.functions, &topological_sort_transitions(cipher), values);
}

pub fn propagate_encryption(functions: &Vec<FunctionNode>, transitions: &VecDeque<Transition>, values: &mut Values) {
    for Transition { inputs, function, outputs } in transitions {
        let input_values = inputs.iter().map(|it| values.get(it).expect(&format!("the value is not computed for {}", it.uid))).collect();
        let fn_node = &functions[function.uid];
        assert!(fn_node.domain.iter().zip_eq(&input_values).all(|(d, v)| d.contains(v)));
        let output_values = fn_node.function.call(&input_values);
        outputs.iter().zip_eq(&output_values).for_each(|(&k, &v)| {
            values.insert(k, v);
        });
        assert!(fn_node.co_domain.iter().zip_eq(&output_values).all(|(d, v)| d.contains( v)));
    }
}

pub fn topological_sort_transitions(dag: &SpecificationGraph) -> VecDeque<Transition> {
    let mut visited = Vec::new();
    let mut ordered_transitions = VecDeque::with_capacity(dag.transitions.len());

    for e in &dag.transitions {
        if !visited.contains(e) {
            visit_edge(dag, e, &mut visited, &mut ordered_transitions);
        }
    }

    ordered_transitions
}

pub fn visit_edge(dag: &SpecificationGraph, e: &Transition, visited: &mut Vec<Transition>, ordered_edges: &mut VecDeque<Transition>) {
    visited.push(e.clone());
    for f in dag.transitions.iter().filter(|it| EdgeWithAnyInputs { inputs: &e.outputs }.call(it)) {
        if !visited.contains(f) {
            visit_edge(dag, f, visited, ordered_edges);
        }
    }
    ordered_edges.push_front(e.clone());
}

pub struct EdgeWithAnyInputs<'r> {
    inputs: &'r Vec<StateNodeUID>,
}

impl<'r> EdgeWithAnyInputs<'r> {
    fn call(&self, edge: &Transition) -> bool {
        self.inputs.iter().any(|it| edge.inputs.contains(it))
    }
}

pub struct Values {
    values: Vec<Option<Value>>,
}

impl Index<&StateNodeUID> for Values {
    type Output = Option<Value>;

    fn index(&self, index: &StateNodeUID) -> &Self::Output {
        &self.values[index.uid]
    }
}

impl Values {
    pub fn new(len: usize) -> Values {
        Values { values: vec![None; len] }
    }

    pub fn get(&self, index: &StateNodeUID) -> Option<Value> {
        self.values[index.uid]
    }

    pub fn insert(&mut self, index: StateNodeUID, value: Value) {
        self.values[index.uid] = Some(value);
    }

    pub fn clear(&mut self) {
        self.values.iter_mut().for_each(|it| *it = None);
    }

    pub fn of(&self, values: &Vec<StateNodeUID>) -> Vec<Value> {
        values.iter().map(|it| self[it].unwrap()).collect()
    }

    pub fn fmt_of(&self, base: &Base, values: &Vec<StateNodeUID>) -> String
    {
        let mut res = String::from('[');
        res.push_str(&values.iter().filter_map(|it| self[it].map(|value| base.format(value))).join(", "));
        res.push(']');
        res
    }
}
