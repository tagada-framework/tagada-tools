use std::collections::HashMap;
use std::fmt::{Display, Formatter, Write};
use std::ops::AddAssign;
use tagada_structs::differential::{DifferentialFunction, DifferentialGraph, DifferentialStateNode, DifferentialStateNodeUID, DifferentialTransition};
use tagada_structs::specification::{Direction, Function, LinearFunction, SpecificationGraph, StateNode, StateNodeUID, Transition};
use tagada_structs::truncated_differential::{TruncatedDifferentialFunction, TruncatedDifferentialGraph, TruncatedDifferentialStateNode, TruncatedDifferentialStateNodeUID, TruncatedDifferentialTransition};

#[derive(Eq, PartialEq, Clone, Debug)]
pub struct Graphviz(pub String);

#[derive(Eq, PartialEq, Clone, Debug)]
struct GraphvizNode {
    uid: String,
    attrs: Option<GraphvizNodeAttributes>,
}

#[derive(Eq, PartialEq, Clone, Debug)]
struct Edge {
    from: GraphvizNode,
    to: GraphvizNode,
}

impl Display for Edge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{} -> {};\n", self.from.uid, self.to.uid))
    }
}

impl Display for GraphvizNode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(attrs) = &self.attrs {
            f.write_fmt(format_args!("{} {};\n", self.uid, attrs))
        } else {
            f.write_fmt(format_args!("{};\n", self.uid))
        }
    }
}

#[derive(Default, Eq, PartialEq, Clone, Debug)]
struct GraphvizNodeAttributes {
    label: Option<String>,
    style: Option<NodeStyle>,
    color: Option<GraphvizColor>,
    shape: Option<NodeShape>,
}

impl GraphvizNodeAttributes {
    #[allow(dead_code)]
    fn with_label(&mut self, label: String) -> &mut GraphvizNodeAttributes {
        self.label = Some(label);
        self
    }
    #[allow(dead_code)]
    fn without_label(&mut self) -> &mut GraphvizNodeAttributes {
        self.label = None;
        self
    }
    #[allow(dead_code)]
    fn with_style(&mut self, style: NodeStyle) -> &mut GraphvizNodeAttributes {
        self.style = Some(style);
        self
    }
    #[allow(dead_code)]
    fn without_style(&mut self) -> &mut GraphvizNodeAttributes {
        self.style = None;
        self
    }
    #[allow(dead_code)]
    fn with_color(&mut self, color: GraphvizColor) -> &mut GraphvizNodeAttributes {
        self.color = Some(color);
        self
    }
    #[allow(dead_code)]
    fn without_color(&mut self) -> &mut GraphvizNodeAttributes {
        self.color = None;
        self
    }
    #[allow(dead_code)]
    fn with_shape(&mut self, shape: NodeShape) -> &mut GraphvizNodeAttributes {
        self.shape = Some(shape);
        self
    }
    #[allow(dead_code)]
    fn without_shape(&mut self) -> &mut GraphvizNodeAttributes {
        self.shape = None;
        self
    }
}


impl Display for GraphvizNodeAttributes {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_char('[')?;
        if let Some(label) = &self.label {
            f.write_fmt(format_args!("label=\"{}\"", label))?;
        }
        if let Some(style) = &self.style {
            if self.label.is_some() { f.write_char(',')?; }
            f.write_fmt(format_args!("style=\"{}\"", style))?;
        }
        if let Some(color) = &self.color {
            if self.label.is_some() || self.style.is_some() { f.write_char(',')?; }
            f.write_fmt(format_args!("color={}", color))?;
        }
        if let Some(shape) = &self.shape {
            if self.label.is_some() || self.style.is_some() || self.color.is_some() { f.write_char(',')?; }
            f.write_fmt(format_args!("shape={}", shape))?;
        }
        f.write_char(']')
    }
}

#[allow(dead_code)]
#[derive(Eq, PartialEq, Copy, Clone, Debug)]
enum NodeShape {
    Box,
    Polygon,
    Ellipse,
    Oval,
    Circle,
    Point,
    Egg,
    Triangle,
    Plaintext,
    Plain,
    Diamond,
    Trapezium,
    Parallelogram,
    House,
    Pentagon,
    Hexagon,
    Septagon,
    Octagon,
}

impl Display for NodeShape {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            NodeShape::Box => f.write_str("box"),
            NodeShape::Polygon => f.write_str("polygon"),
            NodeShape::Ellipse => f.write_str("ellipse"),
            NodeShape::Oval => f.write_str("oval"),
            NodeShape::Circle => f.write_str("circle"),
            NodeShape::Point => f.write_str("point"),
            NodeShape::Egg => f.write_str("egg"),
            NodeShape::Triangle => f.write_str("triangle"),
            NodeShape::Plaintext => f.write_str("plaintext"),
            NodeShape::Plain => f.write_str("plain"),
            NodeShape::Diamond => f.write_str("diamond"),
            NodeShape::Trapezium => f.write_str("trapezium"),
            NodeShape::Parallelogram => f.write_str("parallelogram"),
            NodeShape::House => f.write_str("house"),
            NodeShape::Pentagon => f.write_str("pentagon"),
            NodeShape::Hexagon => f.write_str("hexagon"),
            NodeShape::Septagon => f.write_str("septagon"),
            NodeShape::Octagon => f.write_str("octagon"),
        }
    }
}

#[allow(dead_code)]
#[derive(Eq, PartialEq, Debug, Copy, Clone)]
enum NodeStyle {
    Dashed,
    Dotted,
    Solid,
    Invisible,
    Bold,
    Filled,
}

impl Display for NodeStyle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            NodeStyle::Dashed => f.write_str("dashed"),
            NodeStyle::Dotted => f.write_str("dotted"),
            NodeStyle::Solid => f.write_str("solid"),
            NodeStyle::Invisible => f.write_str("invis"),
            NodeStyle::Bold => f.write_str("bold"),
            NodeStyle::Filled => f.write_str("filled"),
        }
    }
}

#[allow(dead_code)]
#[derive(Eq, PartialEq, Debug, Clone)]
enum GraphvizColor {
    RGB(u8, u8, u8),
    X11(&'static str),
}

impl Display for GraphvizColor {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            GraphvizColor::RGB(r, g, b) => f.write_fmt(format_args!("\"#{:02x}{:02x}{:02x}\"", r, g, b)),
            GraphvizColor::X11(name) => f.write_str(name)
        }
    }
}

const STATE_NODE_STYLE: GraphvizNodeAttributes = GraphvizNodeAttributes {
    label: None,
    style: Some(NodeStyle::Filled),
    color: None,
    shape: Some(NodeShape::Box),
};

const FUNCTION_NODE_STYLE: GraphvizNodeAttributes = GraphvizNodeAttributes {
    label: None,
    style: None,
    color: None,
    shape: Some(NodeShape::Circle),
};

const CONST_STATE_COLOR: GraphvizColor = GraphvizColor::RGB(176, 146, 59);
const PLAINTEXT_STATE_COLOR: GraphvizColor = GraphvizColor::RGB(86, 174, 108);
const KEY_STATE_COLOR: GraphvizColor = GraphvizColor::RGB(137, 96, 179);
const CIPHERTEXT_STATE_COLOR: GraphvizColor = GraphvizColor::RGB(186, 73, 91);
const SBOX_COLOR: GraphvizColor = GraphvizColor::RGB(211, 47, 47);
const XOR_COLOR: GraphvizColor = GraphvizColor::RGB(66, 165, 245);
const GF_MUL: GraphvizColor = GraphvizColor::RGB(245, 218, 66);

fn color_xx_state_node<'a, 'b, 'c, T: PartialEq>(
    attrs: &'a mut GraphvizNodeAttributes,
    blocks: &[(&'b Vec<T>, GraphvizColor)],
    element: &'c T,
) -> &'a mut GraphvizNodeAttributes {
    for (block, color) in blocks {
        if block.contains(element) {
            return attrs.with_color(color.clone());
        }
    }
    attrs
}

fn label_xx_state_node<'a, 'b, 'c, T: PartialEq>(
    attrs: &'a mut GraphvizNodeAttributes,
    names: &'b HashMap<String, T>,
    element: &'c T,
) -> &'a mut GraphvizNodeAttributes {
    for (name, state_node_uid) in names {
        if element == state_node_uid {
            let prefix = attrs.label.clone()
                .map(|it| it + "\n")
                .unwrap_or_default();
            attrs.with_label(prefix + name);
        }
    }
    attrs
}

fn label_xx_state_node_with_prefix<'a, 'b, 'c, T: PartialEq>(
    attrs: &'a mut GraphvizNodeAttributes,
    names: &'b HashMap<String, T>,
    element: &'c T,
    prefix: &'static str,
) -> &'a mut GraphvizNodeAttributes {
    for (name, state_node_uid) in names {
        if element == state_node_uid {
            let prefix = attrs.label.clone()
                .map(|it| it + "\n " + prefix)
                .unwrap_or(String::from(prefix));
            attrs.with_label(prefix + name);
        }
    }
    attrs
}

fn op(uid: usize) -> GraphvizNode {
    GraphvizNode {
        uid: format!("op_{}", uid),
        attrs: None,
    }
}

fn op_with_attributes(uid: usize, attrs: GraphvizNodeAttributes) -> GraphvizNode {
    GraphvizNode {
        uid: format!("op_{}", uid),
        attrs: Some(attrs),
    }
}

fn non_linear_function_style(attrs: &mut GraphvizNodeAttributes) -> &mut GraphvizNodeAttributes {
    attrs.with_style(NodeStyle::Filled)
        .with_color(SBOX_COLOR)
}

fn linear_function_style<'a, 'b>(attrs: &'a mut GraphvizNodeAttributes, linear: &'b LinearFunction) -> &'a mut GraphvizNodeAttributes {
    attrs.with_style(NodeStyle::Filled);
    match linear {
        LinearFunction::Equal => attrs.with_label(String::from("==")),
        LinearFunction::BitwiseXOR => attrs.with_label(String::from("⊕"))
            .with_style(NodeStyle::Filled)
            .with_color(XOR_COLOR),
        LinearFunction::Table { .. } => attrs.with_label(String::from("TAB")),
        LinearFunction::Command { .. } => attrs.with_label(String::from("EXE")),
        LinearFunction::GFMul { .. } => attrs.with_label(String::from("⊗"))
            .with_style(NodeStyle::Filled)
            .with_color(GF_MUL),
        LinearFunction::GFMatToVecMul { .. } => attrs.with_label(String::from("M⊗"))
            .with_style(NodeStyle::Filled)
            .with_color(GF_MUL),
        LinearFunction::LFSR { .. } => attrs.with_label(String::from("LFSR")),
        LinearFunction::Split { .. } => attrs.with_label(String::from("AB=>(A||B)")),
        LinearFunction::Concat { .. } => attrs.with_label(String::from("(A||B)=>AB")),
        LinearFunction::Shift { direction, .. } => match direction {
            Direction::ShiftLeft => attrs.with_label(String::from("<<")),
            Direction::ShiftRight => attrs.with_label(String::from(">>")),
        },
        LinearFunction::CircularShift { direction, .. } => match direction {
            Direction::ShiftLeft => attrs.with_label(String::from("<<<")),
            Direction::ShiftRight => attrs.with_label(String::from(">>>")),
        },
        LinearFunction::AndConst { .. } => attrs.with_label(String::from("&")),
        LinearFunction::OrConst { .. } => attrs.with_label(String::from("|")),
    }
}

impl AddAssign<&GraphvizNode> for String {
    fn add_assign(&mut self, rhs: &GraphvizNode) {
        self.push('\t');
        self.push_str(&rhs.to_string());
    }
}

impl AddAssign<&Edge> for String {
    fn add_assign(&mut self, rhs: &Edge) {
        self.push('\t');
        self.push_str(&rhs.to_string());
    }
}

impl From<&SpecificationGraph> for Graphviz {
    fn from(value: &SpecificationGraph) -> Self {
        fn state(uid: usize) -> GraphvizNode {
            GraphvizNode {
                uid: format!("state_{}", uid),
                attrs: None,
            }
        }
        fn state_with_attributes(uid: usize, attrs: GraphvizNodeAttributes) -> GraphvizNode {
            GraphvizNode {
                uid: format!("state_{}", uid),
                attrs: Some(attrs),
            }
        }

        let mut res = String::from("digraph {\n");
        res += "\tgraph [rankdir=LR];\n";
        res += "\tnode [fontname=\"monospace\"];\n\n";

        let colored_blocks = &[
            (&value.plaintext, PLAINTEXT_STATE_COLOR),
            (&value.key, KEY_STATE_COLOR),
            (&value.ciphertext, CIPHERTEXT_STATE_COLOR),
        ];
        for (uid, state_node) in value.states.iter().enumerate() {
            let uid = StateNodeUID { uid };
            let mut attributes = STATE_NODE_STYLE.clone();
            color_xx_state_node(&mut attributes, colored_blocks, &uid);
            if let StateNode::Constant { value } = state_node {
                attributes
                    .with_shape(NodeShape::Plaintext)
                    .with_color(CONST_STATE_COLOR)
                    .with_label(format!("{:x}", value));
            }
            label_xx_state_node(&mut attributes, &value.names, &uid);
            res += &state_with_attributes(uid.uid, attributes);
        }
        res += "\n";
        for (function_uid, Transition { inputs, function, outputs }) in value.transitions.iter().enumerate() {
            let mut attributes = FUNCTION_NODE_STYLE.clone();
            match &value.functions[function.uid].function {
                Function::SBox { .. } => non_linear_function_style(&mut attributes)
                    .with_label(String::from("S")),
                Function::Linear { linear } => linear_function_style(&mut attributes, linear)
            };
            res += &op_with_attributes(function_uid, attributes);
            for input in inputs {
                res += &Edge { from: state(input.uid), to: op(function_uid) };
            }
            for output in outputs {
                res += &Edge { from: op(function_uid), to: state(output.uid) };
            }
        }

        res += "}\n";
        Graphviz(res)
    }
}

impl From<&DifferentialGraph> for Graphviz {
    fn from(value: &DifferentialGraph) -> Self {
        fn diff_state(uid: usize) -> GraphvizNode {
            GraphvizNode {
                uid: format!("δstate_{}", uid),
                attrs: None,
            }
        }
        fn diff_state_with_attributes(uid: usize, attrs: GraphvizNodeAttributes) -> GraphvizNode {
            GraphvizNode {
                uid: format!("δstate_{}", uid),
                attrs: Some(attrs),
            }
        }

        let mut res = String::from("digraph {\n");
        res += "\tgraph [rankdir=LR];\n";
        res += "\tnode [fontname=\"monospace\"];\n\n";

        let colored_blocks = &[
            (&value.plaintext, PLAINTEXT_STATE_COLOR),
            (&value.key, KEY_STATE_COLOR),
            (&value.ciphertext, CIPHERTEXT_STATE_COLOR),
        ];
        for (uid, state_node) in value.states.iter().enumerate() {
            let uid = DifferentialStateNodeUID { uid };
            let mut attributes = STATE_NODE_STYLE.clone();
            color_xx_state_node(&mut attributes, colored_blocks, &uid);
            if let DifferentialStateNode::Constant { value } = state_node {
                attributes
                    .with_color(CONST_STATE_COLOR)
                    .with_label(format!("{:x}", value));
            }
            label_xx_state_node_with_prefix(&mut attributes, &value.names, &uid, "δ");
            res += &diff_state_with_attributes(uid.uid, attributes);
        }
        res += "\n";
        for (function_uid, DifferentialTransition { inputs, function, outputs }) in value.transitions.iter().enumerate() {
            let mut attributes = FUNCTION_NODE_STYLE.clone();
            match &value.functions[function.uid].function {
                DifferentialFunction::DDT { .. } => non_linear_function_style(&mut attributes)
                    .with_label(String::from("DDT")),
                DifferentialFunction::Linear { linear } => linear_function_style(&mut attributes, linear)
            };
            res += &op_with_attributes(function_uid, attributes);
            for input in inputs {
                res += &Edge { from: diff_state(input.uid), to: op(function_uid) };
            }
            for output in outputs {
                res += &Edge { from: op(function_uid), to: diff_state(output.uid) };
            }
        }

        res += "}\n";
        Graphviz(res)
    }
}

impl From<&TruncatedDifferentialGraph> for Graphviz {
    fn from(value: &TruncatedDifferentialGraph) -> Self {
        fn truncated_diff_state(uid: usize) -> GraphvizNode {
            GraphvizNode {
                uid: format!("Δstate_{}", uid),
                attrs: None,
            }
        }
        fn truncated_diff_state_with_attributes(uid: usize, attrs: GraphvizNodeAttributes) -> GraphvizNode {
            GraphvizNode {
                uid: format!("Δstate_{}", uid),
                attrs: Some(attrs),
            }
        }

        let mut res = String::from("digraph {\n");
        res += "\tgraph [rankdir=LR];\n";
        res += "\tnode [fontname=\"monospace\"];\n\n";

        let colored_blocks = &[
            (&value.plaintext, PLAINTEXT_STATE_COLOR),
            (&value.key, KEY_STATE_COLOR),
            (&value.ciphertext, CIPHERTEXT_STATE_COLOR),
        ];
        for (uid, state_node) in value.states.iter().enumerate() {
            let uid = TruncatedDifferentialStateNodeUID { uid };
            let mut attributes = STATE_NODE_STYLE.clone();
            color_xx_state_node(&mut attributes, colored_blocks, &uid);
            if let TruncatedDifferentialStateNode::Constant { value } = state_node {
                attributes
                    .with_color(CONST_STATE_COLOR)
                    .with_label(format!("{:x}", value));
            }
            label_xx_state_node_with_prefix(&mut attributes, &value.names, &uid, "Δ");
            res += &truncated_diff_state_with_attributes(uid.uid, attributes);
        }
        res += "\n";
        for (function_uid, TruncatedDifferentialTransition { inputs, function, outputs }) in value.transitions.iter().enumerate() {
            let mut attributes = FUNCTION_NODE_STYLE.clone();
            match &value.functions[function.uid].function {
                TruncatedDifferentialFunction::NonLinearTransition { maximum_activation_probability } => non_linear_function_style(&mut attributes)
                    .with_label(format!("2^{{-{}}}", maximum_activation_probability.0.0 as f64 / 100.0)),
                TruncatedDifferentialFunction::Linear { .. } => attributes.with_style(NodeStyle::Filled)
            };
            res += &op_with_attributes(function_uid, attributes);
            for input in inputs {
                res += &Edge { from: truncated_diff_state(input.uid), to: op(function_uid) };
            }
            for output in outputs {
                res += &Edge { from: op(function_uid), to: truncated_diff_state(output.uid) };
            }
        }

        res += "}\n";
        Graphviz(res)
    }
}