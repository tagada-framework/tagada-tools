# Tagada-tools (previously Tagada-lib)

**FOR READERS THAT WANT TO RETRIEVE THE CODE OF [Automatic Generation of Declarative Models for Differential Cryptanalysis](https://hal.science/hal-03320980/file/main.pdf) PLEASE REFER TO THE TAG `1.0` AND PREVIOUS COMMITS.**

Tagada-Tools is a part of the [Tagada](https://gitlab.limos.fr/iia_lulibral/tagada) library, it provides cryptanalysis tools on Specification ciphers.

## Installation

Tagada-Tools is written in [`Rust`](https://www.rust-lang.org/) and thus requires [`Cargo`](https://doc.rust-lang.org/cargo/getting-started/installation.html) to be installed.

### Compiling the sources

```sh
cargo build --release
```

### Dependencies

Tagada-Tools depends on other software for some tasks. When a external dependency is required a `path` option can be used to link the binaries to Tagada-Tools. We will explain it a little bit deeper on the following documentation.

## Usage

Once the binary is compiled it can be found at:

```sh
./target/release/tagada_tools
```

Tagada-Tools provides utilities that are grouped into the following categories:

- `transform`: This category contains the utilities that transform graphs into other graphs;
- `search`: This category contains the utilities that solve cryptanalysis problems;
- `evaluate`: This category contains the utilities that provide automatic tools to evaluate experimentally the solutions found with the tools in the `search` category (when possible);
- `test`: This category contains the utilities that provide automatic tests.

### Transform

**Usage:**

```sh
./target/release/tagada_tools transform <COMMAND>
```

#### `single-key`

Transform a Specification graph into another Specification graph where the Keyschedule is deleted. In the operators that mix the key
with the cipher, the key is replaced by a constant = `0`.

**Usage:**

```sh
./target/release/tagada_tools transform single-key <PATH>
```

The graph can be given from a `PATH` or directly in the standard input (*e.g.* `./generate_cipher.sh | ./target/release/tagada_tools transform single-key`)

**Example:**

```sh
./target/release/tagada_tools transform single-key examples/aes128-3rounds.spec.json > examples/aes128-3rounds.sk.spec.json
```

#### `derive`

Transform a Specification graph into a Differential graph. In the Differential graph, S-Boxes are replaced by DDT and constants in XOR equations are removed.

**Usage:**

```sh
./target/release/tagada_tools transform derive <PATH>
```

The graph can be given from a `PATH` or directly in the standard input.

```sh
./target/release/tagada_tools transform derive examples/aes128-3rounds.spec.json > examples/aes128-3rounds.diff.json
```

#### `truncate-differential`

Transform a Differential graph into a Truncated Differential graph. This process automatically compute the operator abstractions, while this computation is cached, it can take a long time when the process meet a function for the first time.

**Usage:**

```sh
./target/release/tagada_tools transform truncate-differential <OPTIONS> <PATH>
```

**OPTIONS:**

Sometimes the abstraction process loose too much information to be used as is. To counter this drawback we add some features that strengthen the Truncated Differential graph representation.

- `-g`, `--generate-xors`: Generates new XOR equations form the original XOR equations set of the cipher. This parameter takes an `int` value which is the maximal size of the generated equations.
- `-d`, `--diff-variables`: Add differential variables to the graph.
- `-t`, `--transitivity `: Enable transitivity checking for the diff-variables (this option requires that the diff-variables are also active).

The graph can be given from a `PATH` or directly in the standard input.

**Example:**

```sh
./target/release/tagada_tools transform truncate-differential -g 5 -d -t examples/aes128-3rounds.diff.json > examples/aes128-3rounds.trunc.json
```

### Search

#### `best-differential-characteristic`

#### `best-truncated-differential-characteristic`

### Evaluate

#### `differential-distinguisher`

### Test

#### `vectors

