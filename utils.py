import os
import stat
import sys
from functools import partial
from urllib.request import urlretrieve

ASSUME_YES = False

eprint = partial(print, file=sys.stderr)


def run_all(functions):
    for func in functions:
        func()


def ask_yes_no(question, default=None):
    if ASSUME_YES:
        return True
    answer = "_"
    valid_answers = ["y", "n", "Y", "N"]
    default_is_true = False
    if default is True:
        default_text = " [Y/n] "
        valid_answers.append("")
        default_is_true = True
    elif default is False:
        default_text = " [y/N] "
        valid_answers.append("")
    else:
        default_text = " "
    while answer not in valid_answers:
        answer = input(question + default_text)
    return answer in ["y", "Y"] or (answer == "" and default_is_true)


def set_executable(path):
    permissions = os.stat(path).st_mode
    os.chmod(path, permissions | stat.S_IEXEC)


def download_and_execute(url, local_path, *options):
    urlretrieve(url, local_path)
    set_executable(local_path)
    from shell import Bin
    Bin(os.path.join(".", local_path))(*options)
    os.remove(local_path)
